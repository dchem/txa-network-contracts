// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2023 TXA PTE. LTD.
pragma solidity 0.8.19;

import "forge-std/Script.sol";
import {BaseMigration} from "./BaseMigration.sol";
import {ProcessingChainManager} from "../../../../src/Manager/ProcessingChain/ProcessingChainManager.sol";
import {AssetChainManager} from "../../../../src/Manager/AssetChain/AssetChainManager.sol";
import {AssetChainHyperlane} from "../../../../src/CrossChain/Hyperlane/AssetChainHyperlane.sol";
import {ProcessingChainHyperlane} from "../../../../src/CrossChain/Hyperlane/ProcessingChainHyperlane.sol";
import {Rollup} from "../../../../src/Rollup/Rollup.sol";

contract Migrate is BaseMigration {

    /// @dev Upgrade contracts on testnet.
    function upgradeTestnet() internal setEnv(Cycle.Test) {
        Chains[] memory chains = new Chains[](3);

        chains[0] = Chains.ArbSepolia;
        chains[1] = Chains.Sepolia;
        chains[2] = Chains.BscTest;
        // chains[2] = Chains.MaticMumbai;

        migrate(Chains.ArbSepolia, chains);
    }

    /// @dev Upgrade contracts on mainnet.
    function upgradeMainnet() internal setEnv(Cycle.Prod) {
        Chains[] memory chains = new Chains[](4);

        chains[0] = Chains.Arbitrum;
        chains[1] = Chains.Ethereum;
        chains[2] = Chains.Polygon;
        chains[3] = Chains.Bsc;

        migrate(Chains.Arbitrum, chains);
    }

    address[] newAssetChainManagers = new address[](4);
    address[] newAssetChainReceivers = new address[](4);

    function run() external {
        if (vm.envUint("CYCLE") == 0) {
            upgradeTestnet();
        } else if (vm.envUint("CYCLE") == 2) {
            upgradeMainnet();
        }
    }

    /// @dev Helper to iterate over chains and select forks.
    /// @param processingChain Processing Chain to use
    /// @param assetChains Asset Chains to use
    function migrate(Chains processingChain, Chains[] memory assetChains) private {

        address participatingInterface = vm.envAddress("VALIDATOR_ADDR");
        address processingChainManager = vm.envAddress("PROCESSING_CHAIN_MANAGER");
        address admin = vm.addr(adminPrivateKey);

        vm.selectFork(forkids[processingChain]);

        vm.startBroadcast(adminPrivateKey);

        Rollup newRollup = new Rollup(participatingInterface, processingChainManager);
        ProcessingChainManager pcm = ProcessingChainManager(processingChainManager);
        pcm.replaceRollup(address(newRollup));
        assert(pcm.rollup() == address(newRollup));

        vm.stopBroadcast();

        // Deploy Asset Chain Managers
        for (uint256 i; i < assetChains.length; i++) {
          console.log("Deploying new AssetChainHyperlane on fork: ", uint(forkids[assetChains[i]]));

          vm.selectFork(forkids[assetChains[i]]);

          assert(assetChainManagers[i].code.length > 0);
          assert(portals[i].code.length > 0);

          vm.startBroadcast(adminPrivateKey);

          AssetChainManager acm = AssetChainManager(assetChainManagers[i]);
          AssetChainHyperlane ach = new AssetChainHyperlane(
            admin,
            assetChainManagers[i],
            hyperlaneMailboxes[i],
            uint32(vm.envUint("PROCESSING_CHAINID"))
          );
          ach.setInterchainSecurityModule(interchainSecurityModules[i]);
          acm.replaceReceiver(address(ach));

          newAssetChainReceivers[i] = address(ach);

          vm.stopBroadcast();
        }

        vm.selectFork(forkids[processingChain]);
        vm.startBroadcast(adminPrivateKey);
        ProcessingChainHyperlane pch = new ProcessingChainHyperlane(
          hyperlaneMailboxes[0], // for the processing chain
          admin,
          address(pcm),
          assetChainManagers[0] // this will be the one for the processing chain
        );
        pcm.replaceRelayer(address(pch));
        // get the asset chain manager for the processing chain
        AssetChainManager pacm = AssetChainManager(assetChainManagers[0]);
        pacm.replaceReceiver(address(pch));
        vm.stopBroadcast();

        // then ensure that we have initialised the receiving mailboxes
        vm.startBroadcast(adminPrivateKey);
        pch.setChainIds(evmChainIds, hyperlaneDomainIds);
        pch.setReceiverAddresses(hyperlaneDomainIds, newAssetChainReceivers);
        vm.stopBroadcast();

    }

}