// SPDX-License-Identifier: MIT
// Copyright © 2023 TXA PTE. LTD.
pragma solidity 0.8.19;

import "forge-std/Script.sol";

/* solhint-disable max-states-count */
contract BaseMigration is Script {

    uint256 internal adminPrivateKey;
    address internal ownerAddress;

    address[] internal assetChainManagers = new address[](4);
    address[] internal portals = new address[](4);
    address[] internal hyperlaneMailboxes = new address[](4);
    uint256[] internal evmChainIds = new uint256[](4);
    uint32[] internal hyperlaneDomainIds = new uint32[](4);
    address[] internal interchainSecurityModules = new address[](4);

    enum Chains {
        // Testnet
        ArbSepolia,
        // MaticMumbai, // deprecated network
        BscTest,
        Sepolia,

        // Mainnet
        Arbitrum,
        Polygon,
        Bsc,
        Ethereum
    }

    enum Cycle {
        Dev,
        Test,
        Prod
    }

    /// @dev Mapping of chain enum to rpc url
    mapping(Chains chains => string rpcUrls) public forks;
    mapping(Chains chains => uint256 id) public forkids;

    /// @dev environment variable setup for deployment
    /// @param cycle deployment cycle (dev, test, prod)
    modifier setEnv(Cycle cycle) {
        assetChainManagers[0] = vm.envAddress("ARB_ASSET_CHAIN_MANAGER");
        assetChainManagers[1] = vm.envAddress("ETH_ASSET_CHAIN_MANAGER");
        assetChainManagers[2] = vm.envAddress("BSC_ASSET_CHAIN_MANAGER");
        assetChainManagers[3] = vm.envAddress("MATIC_ASSET_CHAIN_MANAGER");

        portals[0] = vm.envAddress("ARB_PORTAL");
        portals[1] = vm.envAddress("ETH_PORTAL");
        portals[2] = vm.envAddress("BSC_PORTAL");
        portals[3] = vm.envAddress("MATIC_PORTAL");

        hyperlaneMailboxes[0] = vm.envAddress("ARB_HYPERLANE_MAILBOX");
        hyperlaneMailboxes[1] = vm.envAddress("ETH_HYPERLANE_MAILBOX");
        hyperlaneMailboxes[2] = vm.envAddress("BSC_HYPERLANE_MAILBOX");
        hyperlaneMailboxes[3] = vm.envAddress("MATIC_HYPERLANE_MAILBOX");

        evmChainIds[0] = vm.envUint("ARB_CHAINID");
        evmChainIds[1] = vm.envUint("ETH_CHAINID");
        evmChainIds[2] = vm.envUint("BSC_CHAINID");
        evmChainIds[3] = vm.envUint("MATIC_CHAINID");

        hyperlaneDomainIds[0] = uint32(vm.envUint("ARB_HYPERLANE_CHAINID"));
        hyperlaneDomainIds[1] = uint32(vm.envUint("ETH_HYPERLANE_CHAINID"));
        hyperlaneDomainIds[2] = uint32(vm.envUint("BSC_HYPERLANE_CHAINID"));
        hyperlaneDomainIds[3] = uint32(vm.envUint("MATIC_HYPERLANE_CHAINID"));

        interchainSecurityModules[0] = vm.envAddress("ARB_INTERCHAIN_SECURITY_MODULE");
        interchainSecurityModules[1] = vm.envAddress("ETH_INTERCHAIN_SECURITY_MODULE");
        interchainSecurityModules[2] = vm.envAddress("BSC_INTERCHAIN_SECURITY_MODULE");
        interchainSecurityModules[3] = vm.envAddress("MATIC_INTERCHAIN_SECURITY_MODULE");

        if (cycle == Cycle.Dev) {
            adminPrivateKey = vm.envUint("LOCAL_DEPLOYER_KEY");
            ownerAddress = vm.envAddress("LOCAL_OWNER_ADDRESS");
        } else if (cycle == Cycle.Test) {
            adminPrivateKey = vm.envUint("TEST_PRIVATE_KEY");
            ownerAddress = vm.envAddress("TEST_OWNER_ADDRESS");
        } else {
            adminPrivateKey = vm.envUint("DEPLOYER_KEY");
            ownerAddress = vm.envAddress("OWNER_ADDRESS");
        }

        _;
    }

    /// @dev broadcast transaction modifier
    /// @param pk private key to broadcast transaction
    modifier broadcast(uint256 pk) {
        vm.startBroadcast(pk);

        _;

        vm.stopBroadcast();
    }

    constructor() {
        // TODO: local

        // Testnet (mumbai is deprecated)
        forks[Chains.ArbSepolia] = "arbitrumsepolia";
        forks[Chains.Sepolia] = "sepolia";
        forks[Chains.BscTest] = "bsctest";
        // forks[Chains.MaticMumbai] = "mumbai";

        forkids[Chains.ArbSepolia] = createFork(Chains.ArbSepolia);
        forkids[Chains.Sepolia] = createFork(Chains.Sepolia);
        forkids[Chains.BscTest] = createFork(Chains.BscTest);
        // forkids[Chains.MaticMumbai] = createFork(Chains.MaticMumbai);

        // TODO: Mainnet
        forks[Chains.Arbitrum] = "arbitrum";
        forks[Chains.Polygon] = "polygon";
        forks[Chains.Bsc] = "bsc";
        forks[Chains.Ethereum] = "ethereum";

        forkids[Chains.Arbitrum] = createFork(Chains.Arbitrum);
        forkids[Chains.Polygon] = createFork(Chains.Polygon);
        forkids[Chains.Bsc] = createFork(Chains.Bsc);
        forkids[Chains.Ethereum] = createFork(Chains.Ethereum);
    }

    function createFork(Chains chain) public returns (uint256) {
        return vm.createFork(forks[chain]);
    }
}