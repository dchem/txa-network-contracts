#!/bin/bash
set -a
source ./.env.testnet
set +a

# Ensure all required environment variables are set
REQUIRED_VARS=(
  "SEPOLIA_RPC_URL"
  "MUMBAI_RPC_URL"
  "BSC_TESTNET_RPC_URL"
  "ARBITRUM_SEPOLIA_RPC_URL"
  "TEST_PRIVATE_KEY"
  "TEST_OWNER_ADDRESS"
)

for var in "${REQUIRED_VARS[@]}"; do
    if [ -z "${!var}" ]; then
        echo "Error: $var is not set!"
        exit 1
    fi
done

# Export the PRIV_KEY
export PRIVATE_KEY

# Add --broadcast here to actually send the transactions
forge script sol/Migrate.s.sol --multi -vvvv
