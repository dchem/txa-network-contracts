#!/bin/bash
set -a
source ./.env.mainnet
set +a

# Ensure all required environment variables are set
REQUIRED_VARS=(
  "ETHERUM_RPC_URL"
  "POLYGON_RPC_URL"
  "BSC_RPC_URL"
  "ARBITRUM_RPC_URL"
  "PRIVATE_KEY"
  "OWNER_ADDRESS"
)

for var in "${REQUIRED_VARS[@]}"; do
    if [ -z "${!var}" ]; then
        echo "Error: $var is not set!"
        exit 1
    fi
done

# Export the PRIV_KEY
export PRIVATE_KEY

# Add --broadcast here to actually send the transactions
forge script sol/Migrate.s.sol --multi -vvvv
