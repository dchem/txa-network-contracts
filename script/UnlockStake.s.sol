// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2023 TXA PTE. LTD.
pragma solidity 0.8.19;

import "forge-std/Script.sol";
import "../src/Portal/Portal.sol";
import "../src/Staking/Staking.sol";
import "../src/Oracle/Oracle.sol";
import "../src/Rollup/Rollup.sol";
import "../src/Manager/ProcessingChain/ProcessingChainManager.sol";
import "@openzeppelin/contracts/token/ERC20/presets/ERC20PresetFixedSupply.sol";

contract UnlockStake is Script {
  using IdLib for Id;

  uint256[] internal lockIds;
  uint256[] internal depositIds;

  function run() external {
    ProcessingChainManager manager = ProcessingChainManager(vm.envAddress("MANAGER"));
    Rollup rollup = Rollup(manager.rollup());
    Staking staking = Staking(manager.staking());

    uint256 currentEpoch = 0;
    bool fraudPeriodExpired = false;

    // There should be a better way to get this, but I'm not sure what it is
    uint256 validatorPrivateKey = vm.envUint("PRIVATE_KEY");
    vm.startBroadcast(validatorPrivateKey);
    while (!fraudPeriodExpired) {
      uint256 _lastConfirmedEpoch = Id.unwrap(rollup.lastConfirmedEpoch());
      console.log("LAST CONFIRMED EPOCH: %s", _lastConfirmedEpoch);

      currentEpoch = _lastConfirmedEpoch + 1;

      bytes32 _stateRoot = rollup.proposedStateRoot(Id.wrap(currentEpoch));
      console.log("PROPOSED STATE ROOT FOR EPOCH %s", currentEpoch);
      console.logBytes32(_stateRoot);

      uint256 _proposedAtBlock = rollup.proposalBlock(Id.wrap(currentEpoch), _stateRoot);
      console.log("STATE ROOT PROPOSED AT BLOCK: %s", _proposedAtBlock);

      uint256 _fraudPeriodEnds = _proposedAtBlock + manager.fraudPeriod();
      console.log("FRAUD PERIOD ENDS AT BLOCK: %s", _fraudPeriodEnds);

      console.log("CURRENT BLOCK: %s", block.number);

      if (block.number < _fraudPeriodEnds || _proposedAtBlock == 0) {
        fraudPeriodExpired = true;
      } else {
        rollup.confirmStateRoot();
        console.log("STATE ROOT FOR EPOCH %s CONFIRMED", currentEpoch);
        currentEpoch++;
      }
    }

    uint256 _nextLockId = Id.unwrap(staking.currentLockId());
    uint256 _nextUnlockId = Id.unwrap(staking.nextIdToUnlock());
    for (uint256 i = _nextUnlockId; i < _nextLockId; i++) {
    if (!rollup.isConfirmedLockId(i)) continue;
        lockIds.push(i);
    }

    staking.unlock(lockIds);
    vm.stopBroadcast();
    console.log("UNLOCKED STAKES UP TO EPOCH: %s", currentEpoch);
  }
}
