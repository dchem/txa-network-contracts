// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2023 TXA PTE. LTD.
pragma solidity ^0.8.19;

import "forge-std/Script.sol";
import "@openzeppelin/contracts/token/ERC20/presets/ERC20PresetFixedSupply.sol";
import "../BaseDeploy.sol";
import "../../../src/util/helpers/Token.sol";

contract DeployTestToken is BaseDeploy {
    using stdJson for string;

    function run() external {
        onlyOnProcessingChain();
        uint256 deployerPrivateKey = vm.envUint("PRIVATE_KEY");
        string memory root = vm.projectRoot();
        string memory path = string.concat(root, "/script/token_accounts.json");
        string memory json = vm.readFile(path);
        address[] memory airdrop = json.readAddressArray("$");
        vm.startBroadcast(deployerPrivateKey);
        Token tacen = new Token(airdrop, "Tacen Token", "TXA.a", 18, 100_000_000e18);
        Token usdt = new Token(airdrop, "TetherUSD", "USDT", 6, 100_000_000e6);
        console.log(address(tacen));
        console.log(address(usdt));
        vm.stopBroadcast();
    }
}
