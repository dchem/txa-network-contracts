// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2023 TXA PTE. LTD.
pragma solidity 0.8.19;

import "../BaseDeploy.sol";
import "forge-std/Script.sol";
import "forge-std/StdJson.sol";
import "../../../src/Manager/ProcessingChain/ProcessingChainManager.sol";
import "../../../src/CrossChain/Hyperlane/ProcessingChainHyperlane.sol";

contract AddSupportedChain is BaseDeploy {
    using stdJson for string;

    function run() external {
        onlyOnProcessingChain();
        string memory json = vm.readFile(assetChainContractsPath);
        address assetChainHyperlane = abi.decode(json.parseRaw(".assetChainHyperlane"), (address));
        json = vm.readFile(processingChainContractsPath);
        ProcessingChainManager manager = ProcessingChainManager(abi.decode(json.parseRaw(".manager"), (address)));
        ProcessingChainHyperlane relayer = ProcessingChainHyperlane(manager.relayer());

        vm.startBroadcast(vm.envUint("PRIVATE_KEY"));

        // add chain ID and assetChainHyperlane address
        uint256[] memory evm = new uint256[](1);
        uint32[] memory hyperlane = new uint32[](1);
        address[] memory portal = new address[](1);
        evm[0] = vm.envUint("ASSET_CHAINID");
        hyperlane[0] = uint32(vm.envUint("HYPERLANE_CHAINID_ASSET"));
        portal[0] = assetChainHyperlane;
        relayer.setChainIds(evm, hyperlane);
        relayer.setReceiverAddresses(hyperlane, portal);

        if (!manager.supportedChains(vm.envUint("ASSET_CHAINID"))) {
            // add supported chain
            manager.addSupportedChain(vm.envUint("ASSET_CHAINID"));
        }

        vm.stopBroadcast();
    }
}
