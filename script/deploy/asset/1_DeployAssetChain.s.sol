// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2023 TXA PTE. LTD.
pragma solidity 0.8.19;

import "../BaseDeploy.sol";
import "../../../src/Manager/AssetChain/AssetChainManager.sol";
import "../../../src/CrossChain/Hyperlane/AssetChainHyperlane.sol";
import "../../../src/CrossChain/Hyperlane/ProcessingChainHyperlane.sol";

contract DeployAssetChain is BaseDeploy {
    using stdJson for string;

    function run() external {
        onlyOnAssetChain();

        uint256 deployerPrivateKey = vm.envUint("PRIVATE_KEY");
        vm.startBroadcast(deployerPrivateKey);
        address participatingInterface = vm.envAddress("PARTICIPATING_INTERFACE_ADDR");
        address admin = vm.addr(deployerPrivateKey);

        // Deploy Asset Manager
        AssetChainManager assetChainManager = new AssetChainManager(participatingInterface, admin);

        // Deploy Hyperlane Receiver
        AssetChainHyperlane assetChainHyperlane = new AssetChainHyperlane(admin, address(assetChainManager), vm.envAddress("HYPERLANE_MAILBOX_ASSET"), uint32(vm.envUint("HYPERLANE_CHAINID_PROCESSING")));
        assetChainManager.replaceReceiver(address(assetChainHyperlane));

        address interchainSecurityModule = vm.envAddress("HYPERLANE_ISM");
        assetChainHyperlane.setInterchainSecurityModule(interchainSecurityModule);

        vm.stopBroadcast();

        // Write asset chain contracts to json file
        string memory obj1 = "{}";
        vm.serializeAddress(obj1, "portal", assetChainManager.portal());
        vm.serializeAddress(obj1, "assetChainHyperlane", assetChainManager.receiver());
        vm.writeJson(vm.serializeAddress(obj1, "manager", address(assetChainManager)), assetChainContractsPath);
    }
}
