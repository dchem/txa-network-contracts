// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2023 TXA PTE. LTD.
pragma solidity 0.8.19;

import "../BaseDeploy.sol";
import "../../../src/Manager/ProcessingChain/ProcessingChainManager.sol";
import "../../../src/Manager/AssetChain/AssetChainManager.sol";
import "../../../src/Rollup/Rollup.sol";
import "../../../src/Staking/Staking.sol";
import "../../../src/CrossChain/Hyperlane/ProcessingChainHyperlane.sol";
import "@murky/Merkle.sol";

contract AddArbAssetChain is BaseDeploy {
    using stdJson for string;

    // This script was used to replace the relayer in the ProcessingChainManager
    // contract. It is not used in any of the automated processes but was used
    // for a manual recovery.
    function run() external {
        onlyOnProcessingChain();
        // address protocolTokenProcessingChain = vm.envAddress("PROTOCOL_TOKEN_ADDR");
        // address stablecoinProcessingChain = vm.envAddress("STABLECOIN_ADDR");
        string memory json = vm.readFile(processingChainContractsPath);
        ProcessingChainManager pcm = ProcessingChainManager(abi.decode(json.parseRaw(".manager"), (address)));
        AssetChainManager acm = AssetChainManager(abi.decode(json.parseRaw(".assetManager"), (address)));
        address hyperlaneMailbox = vm.envAddress("HYPERLANE_MAILBOX_PROCESSING");
        // address hyperlaneMailbox = 0xF4bEE309e322eA3fF01ED6d962d04B2D7da11F7a;
        address owner = vm.envAddress("ADMIN_ADDR");
        address assetManager = abi.decode(json.parseRaw(".assetManager"), (address));

        vm.startBroadcast(vm.envUint("PRIVATE_KEY"));
        ProcessingChainHyperlane pch = new ProcessingChainHyperlane(
            hyperlaneMailbox,
            owner,
            address(pcm),
            address(acm)
        );

        pcm.replaceRelayer(address(pch));
        acm.replaceReceiver(address(pch));
        uint256[] memory evmchainids = new uint256[](4);
        evmchainids[0] = 421614;
        evmchainids[1] = 11155111;
        evmchainids[2] = 80001;
        evmchainids[3] = 97;
        uint32[] memory hyperlanechainids = new uint32[](4);
        hyperlanechainids[0] = 421614;
        hyperlanechainids[1] = 11155111;
        hyperlanechainids[2] = 80001;
        hyperlanechainids[3] = 97;
        pch.setChainIds(evmchainids, hyperlanechainids);
        address[] memory receivers = new address[](4);
        receivers[0] = address(pch);
        receivers[1] = 0xf1aCe9bB42a7AED2EbE27969A0193681B8782358;
        receivers[2] = 0xfd4D5e3A6CDB679F4F8dea369D95D82405c7FE65;
        receivers[3] = 0xB76e607a07f10fB42444f1815B9b08ef7aAf6A68;
        pch.setReceiverAddresses(hyperlanechainids, receivers);

        vm.stopBroadcast();
    }
}
