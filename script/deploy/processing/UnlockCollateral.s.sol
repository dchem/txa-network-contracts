// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2023 TXA PTE. LTD.
pragma solidity 0.8.19;

import "../BaseDeploy.sol";
import "../../../src/Manager/ProcessingChain/ProcessingChainManager.sol";
import "../../../src/Staking/Staking.sol";

// This script is used to unlock collateral in the Staking contract that was locked for settlement.
// This function is permissionless and does not require special privileges to execute.
//
// Required env vars:
// - PRIVATE_KEY: private key of any account that has funds to pay for gas.
contract UnlockCollateral is BaseDeploy {
    using stdJson for string;

    function run() external {
        onlyOnProcessingChain();
        string memory json = vm.readFile(processingChainContractsPath);
        ProcessingChainManager manager = ProcessingChainManager(abi.decode(json.parseRaw(".manager"), (address)));
        Staking staking = Staking(manager.staking());
        uint256 nextIdToUnlock = Id.unwrap(staking.nextIdToUnlock());
        uint256 lastId = nextIdToUnlock;
        for (;; lastId++) {
            (uint256 amountLocked, uint256 totalAmountStaked, uint256 blockNumber, address asset) =
                staking.locks(lastId);
            // This lock ID is not available yet, break out of loop with previous id as final id to unlock
            if (amountLocked > 0 && blockNumber + manager.fraudPeriod() > block.number) {
                break;
            }
        }
        uint256[] memory _lockIds = new uint256[](lastId - nextIdToUnlock);
        for (uint256 i = 0; i < _lockIds.length; i++) {
            _lockIds[i] = nextIdToUnlock + i;
        }

        vm.startBroadcast(vm.envUint("PRIVATE_KEY"));
        staking.unlock(_lockIds);
        vm.stopBroadcast();
    }
}
