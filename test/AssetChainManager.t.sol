// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2023 TXA PTE. LTD.
pragma solidity 0.8.19;

import "./util/BaseTest.sol";

// dumb contract that implements the expected parts of the erc20 interface, but
// doesn't do what you would expect
contract NonStandardERC20 {
  uint256 public decimals;
  mapping(address => uint256) public balances;
  address private ignoreTransfer;
  address private ignoreTransferFrom;
  constructor (uint256 _decimals, address _ignoreTransfer, address _ignoreTransferFrom) {
    decimals = _decimals;
    ignoreTransfer = _ignoreTransfer;
    ignoreTransferFrom = _ignoreTransferFrom;
  }
  function balanceOf(address _who) external returns (uint256) {
    return balances[_who];
  }
  function approve(address _who, uint256 _amount) external {}
  function transfer(address _to, uint256 _amount) external {
    if (msg.sender != ignoreTransfer) {
      balances[msg.sender] -= _amount;
      balances[_to] += _amount;
    }
  }
  function transferFrom(address _from, address _to, uint256 _amount) external {
    if (msg.sender != ignoreTransferFrom) {
      balances[_from] -= _amount;
      balances[_to] += _amount;
    }
  }
}

contract AssetChainManagerTest is BaseTest {
    using IdLib for Id;

    function setUp() public override {
        super.setUp();
    }

    function test_transferAdmin(address _newAdmin) external {
      vm.assume(_newAdmin != address(0));
      vm.prank(admin);
      assetChainManager.transferAdmin(_newAdmin);
      assertEq(assetChainManager.newAdmin(), _newAdmin);
    }

    function test_transferAdminUnauthorised(address _caller) external {
      vm.assume(_caller != admin);
      vm.prank(_caller);
      vm.expectRevert();
      assetChainManager.transferAdmin(alice);
    }

    function test_transferAdminToZero() external {
      vm.prank(admin);
      vm.expectRevert();
      assetChainManager.transferAdmin(address(0));
    }

    function test_transferAdminTwice(address _first, address _second) external {
      vm.assume(_first != address(0)); // second could be anything
      vm.startPrank(admin);
      assetChainManager.transferAdmin(_first);
      vm.expectRevert();
      assetChainManager.transferAdmin(_second);
    }

    function test_cancelAdminTransfer(address _newAdmin) external {
      vm.assume(_newAdmin != address(0));
      vm.startPrank(admin);
      assetChainManager.cancelAdminTransfer(); // we can do this anytime
      assertEq(assetChainManager.newAdmin(), address(0));
      assetChainManager.transferAdmin(_newAdmin);
      assertEq(assetChainManager.newAdmin(), _newAdmin);
      assetChainManager.cancelAdminTransfer();
      assertEq(assetChainManager.newAdmin(), address(0));
      vm.stopPrank();
    }

    function test_cancelAdminTransferUnauthorised(address _caller) external {
      vm.assume(_caller != admin);
      vm.prank(_caller);
      vm.expectRevert();
      assetChainManager.cancelAdminTransfer();
    }

    function test_acceptAdminTransfer(address _newAdmin) external {
      vm.assume(_newAdmin != address(0));
      vm.prank(admin);
      assetChainManager.transferAdmin(_newAdmin);
      vm.prank(_newAdmin);
      assetChainManager.acceptAdminTransfer();
      assertEq(assetChainManager.admin(), _newAdmin);
      assertEq(assetChainManager.newAdmin(), address(0));
    }

    function test_acceptAdminTransferUnauthorised(address _newAdmin, address _caller) external {
      vm.assume(_newAdmin != address(0));
      vm.assume(_caller != _newAdmin);
      vm.prank(admin);
      assetChainManager.transferAdmin(_newAdmin);
      vm.prank(_caller);
      vm.expectRevert();
      assetChainManager.acceptAdminTransfer();
    }

    function test_replaceParticipatingInterface(address _newParticipatingInterface) external {
      // TODO: _newParticipatingInterface can be address(0); - do we care?
      vm.prank(admin);
      assetChainManager.replaceParticipatingInterface(_newParticipatingInterface);
      assertEq(assetChainManager.participatingInterface(), _newParticipatingInterface);
    }

    function test_replaceParticipatingInterfaceUnauthorised(address _newParticipatingInterface, address _caller) external {
      vm.assume(_caller != admin);
      vm.prank(_caller);
      vm.expectRevert();
      assetChainManager.replaceParticipatingInterface(_newParticipatingInterface);
    }

    function test_replaceReceiver(address _newReceiver) external {
      vm.prank(admin);
      assetChainManager.replaceReceiver(_newReceiver);
      assertEq(assetChainManager.receiver(), _newReceiver);
    }

    function test_replaceReceiverUnauthorised(address _newReceiver, address _caller) external {
      vm.assume(_caller != admin);
      vm.prank(_caller);
      vm.expectRevert();
      assetChainManager.replaceReceiver(_newReceiver);
    }

    function test_deployReceiver(uint16 _chainId) external {
      // the assetChainManager from basetest already has a receiver deployed, so
      // we deploy a new one to test this function
      AssetChainManager acm = new AssetChainManager(participatingInterface, admin);
      vm.prank(admin);
      acm.deployReceiver(address(hyperlaneMockMailbox), _chainId);
      assertTrue(acm.receiver() != address(0));
    }

    // function test_deployReceiverNotContract(address _receiver, uint16 _chainId) external {
    //   uint256 size;
    //   assembly {
    //     size := extcodesize(_receiver)
    //   }
    //   vm.assume(size == 0);
    //   AssetChainManager acm = new AssetChainManager(participatingInterface, admin);
    //   vm.prank(admin);
    //   vm.expectRevert();
    //   acm.deployReceiver(_receiver, _chainId);
    // }

    function test_deployReceiverUnauthorised(address _caller) external {
      vm.prank(_caller);
      vm.expectRevert();
      assetChainManager.deployReceiver(address(1), 1);
    }

    function test_deployReceiverTwice() external {
      vm.prank(admin);
      vm.expectRevert();
      assetChainManager.deployReceiver(address(1), 1);
    }

    function test_addSupportedAsset() external {
      ERC20 newToken = new ERC20("Foo", "BAR");
      deal({ token: address(newToken), to: alice, give: 1 });
      vm.prank(alice);
      newToken.approve(address(assetChainManager), 1);
      vm.prank(admin);
      assetChainManager.addSupportedAsset(address(newToken), alice);
      assertEq(assetChainManager.supportedAsset(address(newToken)), true);
    }

    function test_addSupportedAssetUnauthorised() external {
      vm.prank(alice);
      vm.expectRevert();
      assetChainManager.addSupportedAsset(address(1), alice);
    }

    function test_addSupportedAssetDuplicate() external {
      ERC20 newToken = new ERC20("Foo", "BAR");
      deal({ token: address(newToken), to: alice, give: 1 });
      vm.prank(alice);
      newToken.approve(address(assetChainManager), 1);
      vm.prank(admin);
      assetChainManager.addSupportedAsset(address(newToken), alice);
      assertEq(assetChainManager.supportedAsset(address(newToken)), true);
      vm.prank(admin);
      vm.expectRevert();
      assetChainManager.addSupportedAsset(address(newToken), alice);
    }

    function test_addSupportedAssetNoDecimals() external {
      NonStandardERC20 newToken = new NonStandardERC20(0, address(0), address(0));
      deal({ token: address(newToken), to: alice, give: 1 });
      vm.prank(admin);
      vm.expectRevert();
      assetChainManager.addSupportedAsset(address(newToken), alice);
    }

    function test_addSupportedAssetNoTransferFrom() external {
      NonStandardERC20 newToken = new NonStandardERC20(10, address(0), address(assetChainManager));
      deal({ token: address(newToken), to: alice, give: 1 });
      vm.prank(admin);
      vm.expectRevert();
      assetChainManager.addSupportedAsset(address(newToken), alice);
    }

    function test_addSupportedAssetNoTransfer() external {
      NonStandardERC20 newToken = new NonStandardERC20(10, address(assetChainManager), address(0));
      deal({ token: address(newToken), to: alice, give: 1 });
      vm.prank(admin);
      vm.expectRevert();
      assetChainManager.addSupportedAsset(address(newToken), alice);
    }

    function test_setMinimumDeposit(uint256 _minimum) external {
      console.log(_minimum);
      ERC20 newToken = new ERC20("Foo", "BAR");
      deal({ token: address(newToken), to: alice, give: 1 });
      vm.prank(alice);
      newToken.approve(address(assetChainManager), 1);
      vm.startPrank(admin);
      assetChainManager.addSupportedAsset(address(newToken), alice);
      assetChainManager.setMinimumDeposit(address(newToken), _minimum);
      vm.stopPrank();
      if (_minimum == 0) {
        // TODO: 1e6 should be replaced by reading the var from the contract but
        // right now it's not public
        assertEq(assetChainManager.getMinimumDeposit(address(newToken)), 1e6);
      } else {
        assertEq(assetChainManager.getMinimumDeposit(address(newToken)), _minimum);
      }
    }

    function test_setMinimumDepositUnauthorised(address _caller) external {
      vm.assume(_caller != assetChainManager.admin());
      vm.prank(_caller);
      vm.expectRevert();
      assetChainManager.setMinimumDeposit(address(1), 1);
    }

    function test_setMinimumDepositUnsupportedAsset(address _asset) external {
      vm.assume(_asset != address(0) && _asset != address(token));
      vm.startPrank(admin);
      vm.expectRevert();
      assetChainManager.setMinimumDeposit(_asset, 1);
    }

    function test_getMinimumDeposit() external {
      vm.prank(admin);
      assetChainManager.setMinimumDeposit(address(token), 1);
      assertEq(assetChainManager.getMinimumDeposit(address(token)), 1);
      vm.prank(admin);
      assetChainManager.setMinimumDeposit(address(token), 0);
      assertEq(assetChainManager.getMinimumDeposit(address(token)), 1e6);
    }

    function test_getMinimumDepositUnsupportedAsset(address _asset) external {
      vm.assume(assetChainManager.supportedAsset(_asset) == false);
      vm.expectRevert();
      assetChainManager.getMinimumDeposit(_asset);
    }
}
