// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2023 TXA PTE. LTD.
pragma solidity 0.8.19;

import "./util/BaseTest.sol";

contract ProcessingChainHyperlanePublic is ProcessingChainHyperlane {
  constructor(
    address _hyperlaneMailbox,
    address _admin,
    address _pManager,
    address _aManager
  ) ProcessingChainHyperlane(_hyperlaneMailbox, _admin, _pManager, _aManager) {}
  function _chainIds(uint256 evm) public view returns (uint32 hyperlane) {
    return chainIds[evm];
  }
}

contract ProcessingChainHyperlaneTest is BaseTest {
    using IdLib for Id;

    function setUp() public override {
        super.setUp();
    }

    function test_setChainIds(uint8 _number, uint256 _evmChainId, uint32 _hyperlaneChainId) external {
      ProcessingChainHyperlanePublic _processingChainHyperlane = new ProcessingChainHyperlanePublic(
        address(hyperlaneMockMailbox),
        admin,
        address(manager),
        address(assetChainManager)
      );

      uint256[] memory evmIds = new uint256[](_number);
      uint32[] memory hyperlaneIds = new uint32[](_number);

      // this is not super smart, but it should test the property nonetheless
      for (uint8 i = 0; i < _number; i++) {
        evmIds[i] = _evmChainId << i;
        evmIds[i] = _hyperlaneChainId << i;
      }

      vm.prank(admin);
      _processingChainHyperlane.setChainIds(evmIds, hyperlaneIds);

      for (uint256 i = 0; i < _number; i++) {
        assertEq(_processingChainHyperlane._chainIds(evmIds[i]), hyperlaneIds[i]);
      }
    }

    function test_setChainIdsUnauthorized(address _caller) external {
      uint256[] memory evmIds = new uint256[](1);
      uint32[] memory hyperlaneIds = new uint32[](1);
      evmIds[0] = 100;
      hyperlaneIds[0] = 100;
      vm.assume(_caller != processingChainHyperlane.owner());
      vm.prank(_caller);
      vm.expectRevert();
      processingChainHyperlane.setChainIds(evmIds, hyperlaneIds);
    }

    function test_setChainIdsArrayLengthMismatch() external {
      uint256[] memory evmIds = new uint256[](1);
      uint32[] memory hyperlaneIds = new uint32[](2);
      evmIds[0] = 100;
      hyperlaneIds[0] = 100;
      hyperlaneIds[1] = 200;
      vm.prank(admin);
      vm.expectRevert();
      processingChainHyperlane.setChainIds(evmIds, hyperlaneIds);
    }

    function test_sendObligations() external {
      IPortal.Obligation[] memory obligations = new IPortal.Obligation[](1);
      obligations[0] = IPortal.Obligation(alice, address(stablecoin), 10 ether);

      // this is a bit weird because in proactice this function is called via
      // either the staking contract or the rollup contract, and neither hold
      // ETH, so the msg.value comes from the tx.origin which would probably be
      // a validator. in this case we're pranking, so first top up the rollup.
      vm.deal(address(rollup), 10 ether);
      vm.prank(address(rollup));
      processingChainHyperlane.sendObligations{value: 0.5 ether}(1, obligations, address(0));
    }

    function test_sendObligationsLzInterface() external {
      IPortal.Obligation[] memory obligations = new IPortal.Obligation[](1);
      obligations[0] = IPortal.Obligation(alice, address(stablecoin), 10 ether);

      vm.deal(address(rollup), 10 ether);
      vm.prank(address(rollup));
      processingChainHyperlane.sendObligations{value: 0.5 ether}(1, obligations, bytes(""), address(0));
    }

    function test_sendObligationsNoChainId() external {
      uint256[] memory evmIds = new uint256[](1);
      uint32[] memory hyperlaneIds = new uint32[](1);
      evmIds[0] = 1;
      hyperlaneIds[0] = 0;
      vm.prank(admin);
      processingChainHyperlane.setChainIds(evmIds, hyperlaneIds);
      IPortal.Obligation[] memory obligations = new IPortal.Obligation[](1);
      obligations[0] = IPortal.Obligation(alice, address(stablecoin), 10 ether);
      vm.deal(address(rollup), 10 ether);
      vm.prank(address(rollup));
      vm.expectRevert();
      processingChainHyperlane.sendObligations{value: 0.5 ether}(1, obligations, address(0));
    }

    function test_sendObligationsSameChain() external {
      IPortal.Obligation[] memory obligations = new IPortal.Obligation[](1);
      obligations[0] = IPortal.Obligation(alice, address(stablecoin), 10 ether);
      vm.deal(address(rollup), 10 ether);
      vm.prank(address(rollup));

      vm.mockCall(
        address(portal),
        abi.encodeWithSelector(Portal.writeObligations.selector, obligations),
        abi.encode(true)
      );

      processingChainHyperlane.sendObligations{value: 0.5 ether}(31337, obligations, address(0));
    }

    function test_sendObligationsUnauthorised(address _caller) external {
      IPortal.Obligation[] memory obligations = new IPortal.Obligation[](1);
      obligations[0] = IPortal.Obligation(alice, address(stablecoin), 10 ether);
      vm.assume(_caller != address(rollup) && _caller != address(staking));
      vm.prank(_caller);
      vm.expectRevert();
      processingChainHyperlane.sendObligations(1, obligations, address(0));
    }

    function test_sendDepositRejections() external {
      bytes32[] memory deposits = new bytes32[](1);
      deposits[0] = keccak256("foo");
      vm.prank(address(rollup));
      vm.expectRevert();
      processingChainHyperlane.sendDepositRejections{value: 0.5 ether}(1, deposits, address(0));
    }

    function test_sendDepositRejectionsLzInterface() external {
      bytes32[] memory deposits = new bytes32[](1);
      deposits[0] = keccak256("foo");
      vm.prank(address(rollup));
      vm.expectRevert();
      processingChainHyperlane.sendDepositRejections{value: 0.5 ether}(1, deposits, bytes(""), address(0));
    }

    function test_sendDepositRejectionsUnauthorised(address _caller) external {
      vm.assume(_caller != address(rollup));
      bytes32[] memory deposits = new bytes32[](1);
      deposits[0] = keccak256("foo");
      vm.prank(_caller);
      vm.expectRevert();
      processingChainHyperlane.sendDepositRejections{value: 0.5 ether}(1, deposits, address(0));
    }

    function test_sendDepositRejectionsNoChainId() external {
      uint256[] memory evmIds = new uint256[](1);
      uint32[] memory hyperlaneIds = new uint32[](1);
      evmIds[0] = 1;
      hyperlaneIds[0] = 0;
      vm.prank(admin);
      processingChainHyperlane.setChainIds(evmIds, hyperlaneIds);
      bytes32[] memory deposits = new bytes32[](1);
      deposits[0] = keccak256("foo");
      vm.expectRevert();
      processingChainHyperlane.sendDepositRejections{value: 0.5 ether}(1, deposits, address(0));
    }

    function test_sendDepositRejectionsSameChain() external {
      bytes32[] memory deposits = new bytes32[](1);
      deposits[0] = keccak256("foo");
      vm.deal(address(rollup), 0.5 ether);
      vm.prank(address(rollup));

      vm.mockCall(
        address(portal),
        abi.encodeWithSelector(Portal.rejectDeposits.selector, deposits),
        abi.encode(true)
      );

      processingChainHyperlane.sendDepositRejections{value: 0.5 ether}(31337, deposits, address(0));
    }

}
