// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2023 TXA PTE. LTD.
pragma solidity 0.8.19;

import "@Hyperlane/libs/TypeCasts.sol";
import "./util/BaseTest.sol";

contract AssetChainHyperlanePublic is AssetChainHyperlane {

  constructor (
    address _admin,
    address _assetChainManager,
    address _hyperlaneEndpoint,
    uint16 _processingChainId
  ) AssetChainHyperlane(_admin, _assetChainManager, _hyperlaneEndpoint, _processingChainId) {}

  function _handle (
    uint32 _origin,
    bytes32 _sender,
    bytes calldata _data
  ) external {
    this.handle(_origin, _sender, _data);
  }
}

contract AssetChainHyperlaneTest is BaseTest {
    using IdLib for Id;

    AssetChainHyperlanePublic public assetChainHyperlanePublic;
    uint16 public srcChainId = 12;

    function setUp() public override {
      super.setUp();
      vm.prank(address(assetChainManager));
      assetChainHyperlanePublic = new AssetChainHyperlanePublic(admin, address(assetChainManager), address(hyperlaneMockMailbox), srcChainId);
      vm.prank(admin);
      assetChainManager.replaceReceiver(address(assetChainHyperlanePublic));
    }

    function test__handleWriteObligations() external {
      IPortal.Obligation[] memory obligations = new IPortal.Obligation[](1);
      obligations[0].recipient = alice;
      obligations[0].asset = address(0);
      obligations[0].amount = 1 ether;

      // we tell the vm to return address(1) for the portal contract
      vm.mockCall(
        address(assetChainManager),
        abi.encodeWithSelector(assetChainManager.portal.selector),
        abi.encode(address(1))
      );
      // we ensure calls to writeobligations succeed at our mocked portal
      vm.mockCall(
        address(1),
        abi.encodeWithSelector(portal.writeObligations.selector, obligations),
        abi.encode(true)
      );
      // we expect our portal to be called with the correct obligations
      vm.expectCall(
        address(1),
        abi.encodeWithSelector(portal.writeObligations.selector, obligations)
      );

      bytes memory payload = abi.encode(CrossChainFunctions.CrossChainMessage(0, abi.encode(obligations)));
      vm.prank(address(hyperlaneMockMailbox));
      assetChainHyperlanePublic.handle(
        srcChainId,
        TypeCasts.addressToBytes32(address(processingChainHyperlane)),
        payload
      );
    }

    function test__handleWriteObligationsUnauthorised() external {
      IPortal.Obligation[] memory obligations = new IPortal.Obligation[](1);
      obligations[0].recipient = alice;
      obligations[0].asset = address(0);
      obligations[0].amount = 1 ether;
      bytes memory payload = abi.encode(CrossChainFunctions.CrossChainMessage(0, abi.encode(obligations)));
      vm.expectRevert();
      assetChainHyperlanePublic.handle(
        srcChainId,
        TypeCasts.addressToBytes32(address(processingChainHyperlane)),
        payload
      );
    }

    function test__handleRejectDeposits() external {
      bytes32[] memory depositHashes = new bytes32[](1);
      depositHashes[0] = bytes32("some deposit hash");

      vm.mockCall(
        address(assetChainManager),
        abi.encodeWithSelector(assetChainManager.portal.selector),
        abi.encode(address(1))
      );
      vm.mockCall(
        address(1),
        abi.encodeWithSelector(portal.rejectDeposits.selector, depositHashes),
        abi.encode(true)
      );
      vm.expectCall(
        address(1),
        abi.encodeWithSelector(portal.rejectDeposits.selector, depositHashes)
      );

      bytes memory payload = abi.encode(CrossChainFunctions.CrossChainMessage(1, abi.encode(depositHashes)));
      vm.prank(address(hyperlaneMockMailbox));
      assetChainHyperlanePublic.handle(
        srcChainId,
        TypeCasts.addressToBytes32(address(processingChainHyperlane)),
        payload
      );
    }

    function test__handleRejectDepositsUnauthorised() external {
      bytes32[] memory depositHashes = new bytes32[](1);
      depositHashes[0] = bytes32("some deposit hash");
      bytes memory payload = abi.encode(CrossChainFunctions.CrossChainMessage(1, abi.encode(depositHashes)));
      vm.expectRevert();
      assetChainHyperlanePublic.handle(
        srcChainId,
        TypeCasts.addressToBytes32(address(processingChainHyperlane)),
        payload
      );
    }

    function test__handleInvalidInstruction() external {
      // 10 is an invalid instruction
      bytes memory payload = abi.encode(CrossChainFunctions.CrossChainMessage(10, bytes("invalid payload")));
      // the function shouldn't revert, it should just not do anything
      vm.prank(address(hyperlaneMockMailbox));
      assetChainHyperlanePublic.handle(
        srcChainId,
        TypeCasts.addressToBytes32(address(processingChainHyperlane)),
        payload
      );
    }

    function test__handleChainIdMismatch(uint16 _srcChainId) external {
      vm.assume(_srcChainId != srcChainId);
      vm.expectRevert();
      vm.prank(address(hyperlaneMockMailbox));
      assetChainHyperlanePublic.handle(
        _srcChainId,
        TypeCasts.addressToBytes32(address(processingChainHyperlane)),
        bytes("")
      );
    }

}
