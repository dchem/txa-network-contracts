// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2023 TXA PTE. LTD.
pragma solidity 0.8.19;

import "./util/BaseTest.sol";

contract ProcessingChainManagerTest is BaseTest {
    using IdLib for Id;

    event TradingFeesProposed(uint256 makerFee, uint256 takerFee);
    event TradingFeesUpdated(Id indexed feeSequenceId, uint256 makerFee, uint256 takerFee);

    function setUp() public override {
        super.setUp();
    }

    function test_transferAdmin(address _newAdmin) external {
      vm.assume(_newAdmin != address(0));
      vm.prank(admin);
      manager.transferAdmin(_newAdmin);
      assertEq(manager.newAdmin(), _newAdmin);
    }

    function test_transferAdminUnauthorised(address _caller) external {
      vm.assume(_caller != admin);
      vm.prank(_caller);
      vm.expectRevert();
      manager.transferAdmin(address(1));
    }

    function test_transferAdminToZero() external {
      vm.prank(admin);
      vm.expectRevert();
      manager.transferAdmin(address(0));
    }

    function test_transferAdminTwice() external {
      vm.startPrank(admin);
      manager.transferAdmin(address(1));
      vm.expectRevert();
      manager.transferAdmin(address(2));
      vm.stopPrank();
    }

    function test_cancelAdminTransfer() external {
      vm.startPrank(admin);
      manager.transferAdmin(address(1));
      manager.cancelAdminTransfer();
      vm.stopPrank();
      assertEq(manager.newAdmin(), address(0));
    }

    function test_cancelAdminTransferUnauthorised(address _caller) external {
      vm.assume(_caller != admin);
      vm.prank(_caller);
      vm.expectRevert();
      manager.cancelAdminTransfer();
    }

    function test_acceptAdminTransfer(address _newAdmin) external {
      vm.assume(_newAdmin != address(0));
      vm.prank(admin);
      manager.transferAdmin(_newAdmin);
      vm.prank(_newAdmin);
      manager.acceptAdminTransfer();
      assertEq(manager.admin(), _newAdmin);
      assertEq(manager.newAdmin(), address(0));
    }

    function test_acceptAdminTransferUnauthorised(address _caller, address _newAdmin) external {
      vm.assume(_caller != _newAdmin);
      vm.assume(_newAdmin != address(0));
      vm.prank(admin);
      manager.transferAdmin(_newAdmin);
      vm.prank(_caller);
      vm.expectRevert();
      manager.acceptAdminTransfer();
    }

    function test_replaceRelayer(address _newRelayer) external {
      vm.assume(_newRelayer != address(0));
      vm.prank(admin);
      manager.replaceRelayer(_newRelayer);
      assertEq(manager.relayer(), _newRelayer);
    }

    function test_replaceRelayerUnauthorised(address _caller) external {
      vm.assume(_caller != admin);
      vm.prank(_caller);
      vm.expectRevert();
      manager.replaceRelayer(address(1));
    }

    function test_replaceRelayerWithZero() external {
      vm.prank(admin);
      vm.expectRevert();
      manager.replaceRelayer(address(0));
    }

    function test_replaceWalletDelegation(address _newWalletDelegation) external {
      vm.assume(_newWalletDelegation != address(0));
      vm.prank(admin);
      manager.replaceWalletDelegation(_newWalletDelegation);
      assertEq(manager.walletDelegation(), _newWalletDelegation);
    }

    function test_replaceWalletDelegationUnauthorised(address _caller) external {
      vm.assume(_caller != admin);
      vm.prank(_caller);
      vm.expectRevert();
      manager.replaceWalletDelegation(address(1));
    }

    function test_replaceWalletDelegationWithZero() external {
      vm.prank(admin);
      vm.expectRevert();
      manager.replaceWalletDelegation(address(0));
    }

    function test_replaceOracle(address _newOracle) external {
      vm.assume(_newOracle != address(0));
      vm.prank(admin);
      manager.replaceOracle(_newOracle);
      assertEq(manager.oracle(), _newOracle);
    }

    function test_replaceOracleUnauthorised(address _caller) external {
      vm.assume(_caller != admin);
      vm.prank(_caller);
      vm.expectRevert();
      manager.replaceOracle(address(1));
    }

    function test_setFraudEngine(address _newFraudEngine) external {
      // basetest already called this method, so we need a new manager to test
      ProcessingChainManager _manager = new ProcessingChainManager({
        _participatingInterface: participatingInterface,
        _admin: admin,
        _validator: validator,
        _stablecoin: address(stablecoin),
        _protocolToken: address(protocolToken)
      });
      assertEq(_manager.fraudEngine(), address(0));
      vm.prank(admin);
      _manager.setFraudEngine(_newFraudEngine);
      assertEq(_manager.fraudEngine(), _newFraudEngine);
    }

    function test_setFraudEngineUnauthorised(address _caller) external {
      vm.assume(_caller != admin);
      vm.prank(_caller);
      vm.expectRevert();
      manager.setFraudEngine(address(1));
    }

    function test_setFraudEngineTwice(address _newFraudEngine) external {
      vm.assume(_newFraudEngine != address(0));
      assertFalse(manager.fraudEngine() == address(0));
      vm.startPrank(admin);
      vm.expectRevert();
      manager.setFraudEngine(_newFraudEngine);
      vm.stopPrank();
    }

    function test_replaceFraudEngine(address _newFraudEngine) external {
      vm.assume(_newFraudEngine != address(0));
      vm.prank(admin);
      manager.replaceFraudEngine(_newFraudEngine);
      assertEq(manager.fraudEngine(), _newFraudEngine);
    }

    function test_replaceFraudEngineUnauthorised(address _caller) external {
      vm.assume(_caller != admin);
      vm.prank(_caller);
      vm.expectRevert();
      manager.replaceFraudEngine(address(1));
    }

    function test_replaceFraudEngineWithZero() external {
      vm.prank(admin);
      vm.expectRevert();
      manager.replaceFraudEngine(address(0));
    }

    function test_setStaking(address _newStaking) external {
      // basetest already called this method, so we need a new manager to test
      ProcessingChainManager _manager = new ProcessingChainManager({
        _participatingInterface: participatingInterface,
        _admin: admin,
        _validator: validator,
        _stablecoin: address(stablecoin),
        _protocolToken: address(protocolToken)
      });
      vm.assume(_newStaking != address(0));
      assertEq(_manager.staking(), address(0));
      vm.prank(admin);
      _manager.setStaking(_newStaking);
      assertEq(_manager.staking(), _newStaking);
    }

    function test_setStakingTwice(address _staking) external {
      // already set in basetest.sol
      vm.prank(admin);
      vm.expectRevert();
      manager.setStaking(_staking);
    }

    function test_setStakingUnauthorised(address _caller) external {
      vm.assume(_caller != admin);
      vm.prank(_caller);
      vm.expectRevert();
      manager.setStaking(address(1));
    }

    function test_replaceStaking(address _newStaking) external {
      vm.assume(_newStaking != address(0));
      vm.prank(admin);
      manager.replaceStaking(_newStaking);
      assertEq(manager.staking(), _newStaking);
    }

    function test_replaceStakingUnauthorised(address _caller) external {
      vm.assume(_caller != admin);
      vm.prank(_caller);
      vm.expectRevert();
      manager.replaceStaking(address(1));
    }

    function test_replaceStakingWithZero() external {
      vm.prank(admin);
      vm.expectRevert();
      manager.replaceStaking(address(0));
    }

    function test_replaceRollup(address _newRollup) external {
      vm.assume(_newRollup != address(0));
      vm.prank(admin);
      manager.replaceRollup(_newRollup);
      assertEq(manager.rollup(), _newRollup);
    }

    function test_replaceRollupUnauthorised(address _caller) external {
      vm.assume(_caller != admin);
      vm.prank(_caller);
      vm.expectRevert();
      manager.replaceRollup(address(1));
    }

    function test_replaceRollupWithZero() external {
      vm.prank(admin);
      vm.expectRevert();
      manager.replaceRollup(address(0));
    }

    function test_replaceParticipatingInterface(address _newParticipatingInterface) external {
      vm.prank(admin);
      manager.replaceParticipatingInterface(_newParticipatingInterface);
      assertEq(manager.participatingInterface(), _newParticipatingInterface);
    }

    function test_replaceParticipatingInterfaceUnauthorised(address _caller) external {
      vm.assume(_caller != admin);
      vm.prank(_caller);
      vm.expectRevert();
      manager.replaceParticipatingInterface(address(1));
    }

    function test_grantValidator(address _validator) external {
      vm.prank(admin);
      manager.grantValidator(_validator);
      assertEq(manager.validators(_validator), true);
    }

    function test_grantValidatorUnauthorised(address _caller, address _validator) external {
      vm.assume(_caller != admin);
      vm.prank(_caller);
      vm.expectRevert();
      manager.grantValidator(_validator);
    }

    function test_revokeValidator(address _validator) external {
      vm.startPrank(admin);
      manager.grantValidator(_validator);
      assertEq(manager.validators(_validator), true);
      manager.revokeValidator(_validator);
      assertEq(manager.validators(_validator), false);
      vm.stopPrank();
    }

    function test_revokeValidatorUnauthorised(address _caller, address _validator) external {
      vm.assume(_caller != admin);
      vm.prank(_caller);
      vm.expectRevert();
      manager.revokeValidator(_validator);
    }

    function test_updateInsuranceFund(address _insuranceFund) external {
      vm.assume(_insuranceFund != address(0));
      vm.prank(admin);
      manager.updateInsuranceFund(_insuranceFund);
      assertEq(manager.insuranceFund(), _insuranceFund);
    }

    function test_updateInsuranceFundToZero() external {
      vm.prank(admin);
      vm.expectRevert();
      manager.updateInsuranceFund(address(0));
    }

    function test_updateInsuranceFundUnauthorised(address _caller, address _insuranceFund) external {
      vm.assume(_caller != admin);
      vm.assume(_insuranceFund != address(0));
      vm.prank(_caller);
      vm.expectRevert();
      manager.updateInsuranceFund(_insuranceFund);
    }

    function test_addSupportedChain(uint256 _chainId) external {
      vm.assume(_chainId != 1); // 1 was added in basetest
      vm.prank(admin);
      manager.addSupportedChain(_chainId);
      assertEq(manager.supportedChains(_chainId), true);
    }

    function test_addSupportedChainUnauthorised(address _caller, uint256 _chainId) external {
      vm.assume(_caller != admin);
      vm.prank(_caller);
      vm.expectRevert();
      manager.addSupportedChain(_chainId);
    }

    function test_addSupportedChainTwice(uint256 _chainId) external {
      vm.assume(_chainId != 1); // 1 was added in basetest
      vm.startPrank(admin);
      manager.addSupportedChain(_chainId);
      assertEq(manager.supportedChains(_chainId), true);
      vm.expectRevert();
      manager.addSupportedChain(_chainId);
      vm.stopPrank();
    }

    function test_addSupportedAsset(uint256 _chainId, address _asset, uint8 _precision) external {
      vm.assume(_chainId != 1);
      vm.assume(_asset != address(0));
      vm.assume(_precision != 0);

      vm.startPrank(admin);
      manager.addSupportedChain(_chainId);
      manager.addSupportedAsset(_chainId, _asset, _precision);
      vm.stopPrank();

      assertEq(manager.supportedAsset(_chainId, _asset), _precision);
      assertEq(manager.isSupportedAsset(_chainId, _asset), true);
    }

    function test_addSupportedAssetUnauthorised(address _caller) external {
      vm.assume(_caller != admin);
      vm.prank(_caller);
      vm.expectRevert();
      manager.addSupportedAsset(1, address(1), 1);
    }

    function test_addSupportedAssetUnsupportedChain(uint256 _chainId) external {
      vm.assume(_chainId != 1);
      vm.prank(admin);
      vm.expectRevert();
      manager.addSupportedAsset(_chainId, address(1), 1);
    }

    function test_addSupportedAssetTwice(uint256 _chainId, address _asset, uint8 _precision) external {
      vm.assume(_chainId != 1);
      vm.assume(_asset != address(0));
      vm.assume(_precision != 0);

      vm.startPrank(admin);
      manager.addSupportedChain(_chainId);
      manager.addSupportedAsset(_chainId, _asset, _precision);
      vm.expectRevert();
      manager.addSupportedAsset(_chainId, _asset, _precision);
      vm.stopPrank();
    }

    function test_addSupportedAssetInvalidPrecision() external {
      vm.prank(admin);
      vm.expectRevert();
      manager.addSupportedAsset(1, address(1), 0);
    }

    function test_updateRootProposalLockAmount(uint256 _rootProposalLockAmount) external {
      vm.assume(_rootProposalLockAmount != 0);
      vm.prank(admin);
      manager.updateRootProposalLockAmount(_rootProposalLockAmount);
      assertEq(manager.rootProposalLockAmount(), _rootProposalLockAmount);
    }

    function test_updateRootProposalLockAmountToZero() external {
      vm.prank(admin);
      vm.expectRevert();
      manager.updateRootProposalLockAmount(0);
    }

    function test_updateRootProposalLockAmountUnauthorised(address _caller) external {
      vm.assume(_caller != admin);
      vm.prank(_caller);
      vm.expectRevert();
      manager.updateRootProposalLockAmount(10);
    }

    // capped to uint32 because of the fee parameters, otherwise the fuzzer
    // gets upset that too many inputs are rejected. can be modified if the fee
    // parameters change
    function test_proposeFees(uint32 _makerFee, uint32 _takerFee) external {
      vm.assume(_makerFee <= manager.MAX_FEE_NUMERATOR());
      vm.assume(_takerFee <= manager.MAX_FEE_NUMERATOR());
      vm.assume(_makerFee == 0 || _makerFee > manager.MIN_FEE_NUMERATOR());
      vm.assume(_takerFee == 0 || _takerFee > manager.MIN_FEE_NUMERATOR());
      vm.prank(participatingInterface);
      vm.expectEmit(true, true, true, true);
      emit TradingFeesProposed(_makerFee, _takerFee);
      manager.proposeFees(_makerFee, _takerFee);
      (uint256 mf, uint256 tf) = manager.proposedFees();
      assertEq(mf, _makerFee);
      assertEq(tf, _takerFee);
    }

    function test_proposeFeesUnauthorised(address _caller) external {
      vm.assume(_caller != participatingInterface);
      vm.expectRevert();
      manager.proposeFees(0, 0);
    }

    function test_updateFees(uint32 _makerFee, uint32 _takerFee) external {
      vm.assume(_makerFee <= manager.MAX_FEE_NUMERATOR());
      vm.assume(_takerFee <= manager.MAX_FEE_NUMERATOR());
      vm.assume(_makerFee == 0 || _makerFee > manager.MIN_FEE_NUMERATOR());
      vm.assume(_takerFee == 0 || _takerFee > manager.MIN_FEE_NUMERATOR());
      vm.startPrank(participatingInterface);
      manager.proposeFees(_makerFee, _takerFee);
      skip(86_400 + 1); // 1 day
      manager.updateFees();
      (uint256 mf, uint256 tf) = manager.currentFees();
      assertEq(mf, _makerFee);
      assertEq(tf, _takerFee);
    }
}
