// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2023 TXA PTE. LTD.
pragma solidity 0.8.19;

import { Test } from "forge-std/Test.sol";

import "../../src/Manager/ProcessingChain/ProcessingChainManager.sol";
import "../../src/Manager/ProcessingChain/WalletDelegation.sol";
import "../../src/Manager/AssetChain/AssetChainManager.sol";
import "../../src/Staking/Staking.sol";
import "../../src/util/Signature.sol";
import "../../src/Oracle/Oracle.sol";
import "../../src/Rollup/TestRollup.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@murky/Merkle.sol";
import "@Hyperlane/mock/MockMailbox.sol";
import "../../src/CrossChain/Hyperlane/AssetChainHyperlane.sol";
import "../../src/CrossChain/Hyperlane/ProcessingChainHyperlane.sol";
import "forge-std/console.sol";

contract BaseTest is Test {
    using IdLib for Id;

    uint256 internal piKey = 0xEC;
    uint256 internal aliceKey = 0xA11CE;
    uint256 internal bobKey = 0xB0B;
    address internal participatingInterface;
    address internal admin;
    address internal validator;
    address internal fraudEngine;

    ProcessingChainManager internal manager;
    AssetChainManager internal assetChainManager;
    Portal internal portal;
    TestRollup internal rollup;
    Staking internal staking;
    ProcessingChainHyperlane internal processingChainHyperlane;
    AssetChainHyperlane internal assetChainHyperlane;
    Oracle internal oracle;
    WalletDelegation internal walletDelegation;
    MockMailbox internal hyperlaneMockMailbox;
    MockMailbox internal hyperlaneMockMailboxDest;

    Signature internal sigUtil;
    Merkle internal merkleLib;

    address internal alice;
    address internal bob;

    ERC20 internal token;
    ERC20 internal stablecoin;
    ERC20 internal protocolToken;

    uint256 internal chainId = 1;

    event DepositUtxo(
        address wallet,
        uint256 amount,
        address token,
        address participatingInterface,
        Id chainSequenceId,
        bytes32 utxo
    );

    // Note that this does not properly set balance or deposit root
    function depositStateUpdate(
        address _trader,
        address _token,
        uint256 _amount,
        Id _chainSequenceId,
        uint256 _stateUpdateId
    ) internal view returns (StateUpdateLibrary.StateUpdate memory) {
        StateUpdateLibrary.Deposit memory deposit = StateUpdateLibrary.Deposit(
            _trader,
            _token,
            participatingInterface,
            _amount,
            _chainSequenceId,
            Id.wrap(chainId)
        );
        StateUpdateLibrary.Balance memory balance = StateUpdateLibrary.Balance(
            _trader,
            _token,
            Id.wrap(chainId),
            _amount
        );
        StateUpdateLibrary.DepositAcknowledgement memory depositAck = StateUpdateLibrary.DepositAcknowledgement(
            deposit,
            ID_ZERO,
            balance,
            balance,
            ID_ZERO,
            keccak256(abi.encode(0)),
            keccak256(abi.encode(0))
        );
        return
            StateUpdateLibrary.StateUpdate(
                StateUpdateLibrary.TYPE_ID_DepositAcknowledgement,
                Id.wrap(_stateUpdateId),
                participatingInterface,
                abi.encode(depositAck)
            );
    }

    // Note that this does not properly set balance or deposit root
    function depositStateUpdate(
        StateUpdateLibrary.Deposit memory _deposit,
        uint256 _stateUpdateId
    ) internal view returns (StateUpdateLibrary.StateUpdate memory) {
        StateUpdateLibrary.Balance memory balance = StateUpdateLibrary.Balance(
            _deposit.trader,
            _deposit.asset,
            Id.wrap(chainId),
            _deposit.amount
        );
        StateUpdateLibrary.DepositAcknowledgement memory depositAck = StateUpdateLibrary.DepositAcknowledgement(
            _deposit,
            ID_ZERO,
            balance,
            balance,
            ID_ZERO,
            keccak256(abi.encode(0)),
            keccak256(abi.encode(0))
        );
        return
            StateUpdateLibrary.StateUpdate(
                StateUpdateLibrary.TYPE_ID_DepositAcknowledgement,
                Id.wrap(_stateUpdateId),
                participatingInterface,
                abi.encode(depositAck)
            );
    }

    function tradeToStateUpdate(
        StateUpdateLibrary.Trade memory _trade,
        uint256 _stateUpdateId
    ) internal view returns (StateUpdateLibrary.StateUpdate memory) {
        return
            StateUpdateLibrary.StateUpdate(
                StateUpdateLibrary.TYPE_ID_Trade,
                Id.wrap(_stateUpdateId),
                participatingInterface,
                abi.encode(_trade)
            );
    }

    function settlementStateUpdate(
        address _trader,
        address _token,
        Id _chainSequenceId,
        Id _settlementId,
        uint256 _stateUpdateId,
        uint256 _amount
    ) internal view returns (StateUpdateLibrary.StateUpdate memory) {
        StateUpdateLibrary.SettlementRequest memory settlementRequest = StateUpdateLibrary.SettlementRequest(
            _trader,
            _token,
            participatingInterface,
            _chainSequenceId,
            Id.wrap(chainId)
        );
        StateUpdateLibrary.Settlement memory settlement = StateUpdateLibrary.Settlement(
            settlementRequest,
            ID_ZERO,
            StateUpdateLibrary.Balance(_trader, _token, Id.wrap(chainId), _amount),
            StateUpdateLibrary.Balance(_trader, _token, Id.wrap(chainId), 0)
        );
        return
            StateUpdateLibrary.StateUpdate(
                StateUpdateLibrary.TYPE_ID_Settlement,
                Id.wrap(_stateUpdateId),
                participatingInterface,
                abi.encode(settlement)
            );
    }

    function feeStateUpdate(
        uint256 _feeSequenceId,
        uint256 _newMakerFee,
        uint256 _newTakerFee,
        uint256 _lastFeeUpdate,
        uint256 _stateUpdateId
    ) internal view returns (StateUpdateLibrary.StateUpdate memory) {
        StateUpdateLibrary.FeeUpdate memory feeUpdate = StateUpdateLibrary.FeeUpdate(
            _feeSequenceId,
            _newMakerFee,
            _newTakerFee,
            _lastFeeUpdate
        );
        return
            StateUpdateLibrary.StateUpdate(
                StateUpdateLibrary.TYPE_ID_FeeUpdate,
                Id.wrap(_stateUpdateId),
                participatingInterface,
                abi.encode(feeUpdate)
            );
    }

    function signStateUpdate(
        StateUpdateLibrary.StateUpdate memory _stateUpdate
    ) internal view returns (StateUpdateLibrary.SignedStateUpdate memory) {
        (uint8 v, bytes32 r, bytes32 s) = vm.sign(piKey, keccak256(abi.encode(_stateUpdate)));
        return StateUpdateLibrary.SignedStateUpdate(_stateUpdate, v, r, s);
    }

    function settlementData(
        StateUpdateLibrary.Deposit[] memory deposits,
        StateUpdateLibrary.StateUpdate memory settlementAck
    )
        internal
        returns (bytes32 stateRoot, StateUpdateLibrary.SignedStateUpdate memory stateUpdate, bytes32[] memory proof)
    {
        stateUpdate = signStateUpdate(settlementAck);

        // Construct merkle tree of signed state updates
        Merkle m = new Merkle();
        bytes32[] memory data = new bytes32[](deposits.length + 1);

        for (uint256 i; i < deposits.length; i++) {
            console.log(i);
            data[i] = keccak256(abi.encode((signStateUpdate(depositStateUpdate(deposits[i], i)))));
        }
        data[deposits.length] = keccak256(abi.encode(stateUpdate));

        // Get state root and proof of the signed state update with settlement message
        proof = m.getProof(data, data.length - 1);
        stateRoot = m.getRoot(data);
        return (stateRoot, stateUpdate, proof);
    }

    function setUp() public virtual {
        vm.roll(17_752_520);

        participatingInterface = vm.addr(piKey);
        // Generate deterministic address for admin and validator from input
        admin = vm.addr(0xAD);
        validator = vm.addr(0xDA);
        fraudEngine = vm.addr(0xFE);

        stablecoin = new ERC20("Stablecoin", "USDT");
        protocolToken = new ERC20("ProtocolToken", "TXA");
        // Use address of USDT on Arbitrum
        bytes memory stablecoinCode = address(stablecoin).code;
        address targetAddr = 0xFd086bC7CD5C481DCC9C85ebE478A1C0b69FCbb9;
        // Deploying the bytecode of the stablecoin contract to the targetAddr address on the test blockchain
        vm.etch(targetAddr, stablecoinCode);
        stablecoin = ERC20(targetAddr);
        // Use addres of TXA on Arbitrum
        bytes memory protocolTokenCode = address(protocolToken).code;
        targetAddr = 0xca84a842116d741190c3782e94fa9b7B7bbcf31b;
        vm.etch(targetAddr, protocolTokenCode);
        protocolToken = ERC20(targetAddr);
        token = new ERC20("TestToken", "TST");

        manager = new ProcessingChainManager({
            _participatingInterface: participatingInterface,
            _admin: admin,
            _validator: validator,
            _stablecoin: address(stablecoin),
            _protocolToken: address(protocolToken)
        });
        assetChainManager = new AssetChainManager(participatingInterface, admin);
        rollup = new TestRollup(participatingInterface, address(manager));
        // it starts a new session where all subsequent transactions and function calls are made as if they were coming from the admin address
        vm.startPrank(admin);
        manager.replaceRollup(address(rollup));
        manager.updateInsuranceFund(validator);
        staking = new Staking(address(manager), address(stablecoin), address(protocolToken));
        manager.setStaking(address(staking));
        manager.setFraudEngine(fraudEngine);


        // TODO: Replace this setup with Hyperlane setup! Will require modifications to the
        // ProcessingChainLz as well!

        hyperlaneMockMailbox = new MockMailbox(uint32(block.chainid));
        hyperlaneMockMailboxDest = new MockMailbox(uint32(chainId));
        ProcessingChainHyperlane relayer = new ProcessingChainHyperlane(
            address(hyperlaneMockMailbox),
            admin,
            address(manager),
            address(assetChainManager)
        );
        manager.replaceRelayer(address(relayer));
        processingChainHyperlane = ProcessingChainHyperlane(manager.relayer());
        uint256[] memory evmChainId = new uint256[](1);
        evmChainId[0] = chainId;
        uint32[] memory hyperlaneChainId = new uint32[](1);
        hyperlaneChainId[0] = uint32(chainId);
        processingChainHyperlane.setChainIds(evmChainId, hyperlaneChainId);
        assetChainHyperlane = new AssetChainHyperlane(admin, address(assetChainManager), address(hyperlaneMockMailboxDest), uint32(block.chainid));
        assetChainManager.replaceReceiver(address(assetChainHyperlane));
        portal = Portal(assetChainManager.portal());
        assetChainHyperlane = AssetChainHyperlane(assetChainManager.receiver());
        address[] memory receivers = new address[](1);
        receivers[0] = address(assetChainHyperlane);
        processingChainHyperlane.setReceiverAddresses(hyperlaneChainId, receivers);
        hyperlaneMockMailbox.addRemoteMailbox(uint32(chainId), hyperlaneMockMailboxDest);
        hyperlaneMockMailboxDest.addRemoteMailbox(uint32(chainId), hyperlaneMockMailboxDest);

        // Setup initial supported assets

        assetChainManager.addSupportedAsset(address(0), address(0));
        deal({ token: address(token), to: admin, give: 1 });
        token.approve(address(assetChainManager), 1);
        assetChainManager.addSupportedAsset(address(token), admin);
        manager.addSupportedChain(chainId);
        manager.addSupportedAsset(chainId, address(0), 18);
        manager.addSupportedAsset(chainId, address(token), 18);
        // Setup oracle with initial prices
        oracle = new Oracle(
            admin, address(manager), address(protocolToken), address(stablecoin), chainId, 0.3e18
        );
        manager.replaceOracle(address(oracle));
        oracle.grantReporter(admin);
        oracle.initializePrice(chainId, address(0), 1895.25e18);
        walletDelegation = new WalletDelegation(participatingInterface, address(manager));
        vm.stopPrank();

        alice = vm.addr(aliceKey);
        bob = vm.addr(bobKey);
        vm.deal(alice, 10 ether);
        vm.deal(bob, 10 ether);
        vm.deal(validator, 10 ether);
        vm.deal(admin, 10 ether);

        sigUtil = new Signature(participatingInterface);
        merkleLib = new Merkle();
    }
}
