// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2023 TXA PTE. LTD.
pragma solidity 0.8.19;

import "./util/BaseTest.sol";

contract StakingTest is BaseTest {
    using IdLib for Id;

    event Staked(address indexed staker, address indexed asset, uint256 amount, uint256 unlockTime, uint256 depositId);
    event WithdrawStaked(address indexed staker, address indexed asset, uint256 amount);
    event Locked(address indexed asset, uint256 amount, uint256 lockId);
    event Unlocked(uint256[] lockIds);
    event RewardAdded(uint256 indexed lockId, uint256 indexed chainId, address indexed asset, uint256 amount);
    event InsurancePaid(uint256 indexed chainId, address indexed asset, uint256 amount);
    event Claimed(address indexed staker, uint256 indexed chainId, address indexed asset, uint256 amount);

    error INSUFFICIENT_COLLATERAL(uint256 amountToLock, uint256 amountLeft);

    uint256 periodLength;
    uint256 activePeriods;
    uint256 maxPeriod;
    uint256[3] tranches;

    function setUp() public override {
      super.setUp();
      periodLength = staking.PERIOD_LENGTH();
      activePeriods = staking.ACTIVE_PERIODS();
      maxPeriod = staking.MAX_PERIOD();
      tranches = staking.getActiveTranches();
      deal({token: address(stablecoin), to: alice, give: 10_000 ether});
      deal({token: address(protocolToken), to: alice, give: 10_000 ether});
      deal({token: address(stablecoin), to: bob, give: 10_000 ether});
      deal({token: address(protocolToken), to: bob, give: 10_000 ether});
      vm.startPrank(alice);
      stablecoin.approve(address(staking), 10_000 ether);
      protocolToken.approve(address(staking), 10_000 ether);
      vm.stopPrank();
      vm.startPrank(bob);
      stablecoin.approve(address(staking), 10_000 ether);
      protocolToken.approve(address(staking), 10_000 ether);
      vm.stopPrank();
    }

    function test_stake() external {
      vm.expectEmit(true, true, true, true);
      emit Staked(alice, address(stablecoin), 10 ether, tranches[0], Id.unwrap(staking.currentDepositId()));
      vm.prank(alice);
      staking.stake(address(stablecoin), 10 ether, tranches[0]);

      assertEq(staking.individualStaked(alice, address(stablecoin)), 10 ether);
      assertEq(staking.totalStaked(address(stablecoin)), 10 ether);
    }

    function test_stakeInvalidAsset() external {
      vm.prank(alice);
      vm.expectRevert();
      staking.stake(address(1), 10 ether, tranches[0]);
    }

    function test_stakeBelowMinimum() external {
      vm.startPrank(alice);
      vm.expectRevert();
      staking.stake(address(stablecoin), 1, tranches[0]);
      vm.expectRevert();
      staking.stake(address(protocolToken), 1, tranches[0]);
      vm.stopPrank();
    }

    function test_stakeInvalidPeriod() external {
      vm.startPrank(alice);

      vm.expectRevert(); // invalid unlock time
      staking.stake(address(stablecoin), 10 ether, tranches[0] + 1);

      vm.expectRevert(); // too far in future
      staking.stake(address(stablecoin), 10 ether, tranches[2] + (periodLength * 3));

      vm.roll(block.number + periodLength);
      vm.expectRevert(); // no longer in this tranche
      staking.stake(address(stablecoin), 10 ether, tranches[0]);
      vm.stopPrank();
    }

    function test_withdraw() external {
      uint256[] memory deposits = new uint256[](2);
      uint256 depositIdStablecoin = Id.unwrap(staking.currentDepositId());
      uint256 depositIdProtocolToken = Id.unwrap(staking.currentDepositId()) + 1;
      deposits[0] = depositIdStablecoin;
      deposits[1] = depositIdProtocolToken;

      vm.startPrank(alice);
      staking.stake(address(stablecoin), 10 ether, tranches[0]);
      staking.stake(address(protocolToken), 1000 ether, tranches[0]);

      vm.roll(tranches[0] + 1);

      // there have been no rewards, so the withdrawn amounts should be exact
      vm.expectEmit(true, true, true, true);
      emit WithdrawStaked(alice, address(stablecoin), 10 ether);
      vm.expectEmit(true, true, true, true);
      emit WithdrawStaked(alice, address(protocolToken), 1000 ether);
      staking.withdraw(deposits);
      vm.stopPrank();
    }

    function test_withdrawUnauthorised() external {
      vm.startPrank(alice);
      uint256[] memory deposits = new uint256[](1);
      uint256 depositIdStablecoin = Id.unwrap(staking.currentDepositId());
      deposits[0] = depositIdStablecoin;
      staking.stake(address(stablecoin), 10 ether, tranches[0]);
      vm.stopPrank();
      vm.prank(bob);
      vm.expectRevert();
      staking.withdraw(deposits);
    }

    function test_withdrawEarly() external {
      uint256[] memory deposits = new uint256[](2);
      uint256 depositIdStablecoin = Id.unwrap(staking.currentDepositId());
      uint256 depositIdProtocolToken = Id.unwrap(staking.currentDepositId()) + 1;
      deposits[0] = depositIdStablecoin;
      deposits[1] = depositIdProtocolToken;

      vm.startPrank(alice);
      staking.stake(address(stablecoin), 10 ether, tranches[0]);
      staking.stake(address(protocolToken), 1000 ether, tranches[0]);

      vm.roll(tranches[0] - 1);

      vm.expectRevert();
      staking.withdraw(deposits);
      vm.stopPrank();
    }

    function test_withdrawDuplicate() external {
      uint256[] memory deposits = new uint256[](2);
      uint256 depositIdStablecoin = Id.unwrap(staking.currentDepositId());
      uint256 depositIdProtocolToken = Id.unwrap(staking.currentDepositId()) + 1;
      deposits[0] = depositIdStablecoin;
      deposits[1] = depositIdProtocolToken;

      vm.startPrank(alice);
      staking.stake(address(stablecoin), 10 ether, tranches[0]);
      staking.stake(address(protocolToken), 1000 ether, tranches[0]);
      vm.stopPrank();

      // we're going to deposit some as bob just so we know that this test is
      // not passing because we don't have enough assets in the contract
      deal({token: address(stablecoin), to: bob, give: 10_000 ether});
      deal({token: address(protocolToken), to: bob, give: 10_000 ether});
      vm.startPrank(bob);
      stablecoin.approve(address(staking), 10_000 ether);
      protocolToken.approve(address(staking), 10_000 ether);
      staking.stake(address(stablecoin), 10_000 ether, tranches[0]);
      staking.stake(address(protocolToken), 10_000 ether, tranches[0]);
      vm.stopPrank();

      vm.roll(tranches[0] + 1);

      vm.startPrank(alice);
      staking.withdraw(deposits);
      vm.expectRevert(); // can't withdraw the same deposit twice
      staking.withdraw(deposits);
      vm.stopPrank();
    }

    function test_withdrawUnavailable() external {
      uint256[] memory deposits = new uint256[](1);
      deposits[0] = Id.unwrap(staking.currentDepositId());

      vm.prank(alice);
      staking.stake(address(stablecoin), 10 ether, tranches[0]);

      vm.roll(block.number + 1);

      vm.prank(address(rollup));
      staking.lock(address(stablecoin), 10 ether);

      vm.roll(tranches[0] + 1);

      vm.prank(alice);
      vm.expectRevert();
      staking.withdraw(deposits);
    }

    function test_lock() external {
      vm.prank(alice);
      staking.stake(address(stablecoin), 10 ether, tranches[0]);

      uint256 lockId = Id.unwrap(staking.currentLockId());

      vm.prank(address(rollup));
      vm.expectEmit(true, true, true, true);
      emit Locked(address(stablecoin), 10 ether, 0);
      staking.lock(address(stablecoin), 10 ether);

      (uint256 _amountLocked, uint256 _totalAmountStaked, uint256 _blockNumber, address _asset)
        = staking.locks(lockId);

      lockId++;

      assertEq(_amountLocked, 10 ether);
      assertEq(_totalAmountStaked, 10 ether);
      assertEq(_blockNumber, block.number);
      assertEq(_asset, address(stablecoin));
      assertEq(Id.unwrap(staking.currentLockId()), lockId);
    }

    function test_lockUnauthorised(address _locker) external {
      vm.assume(_locker != address(rollup));
      vm.prank(_locker);
      vm.expectRevert();
      staking.lock(address(stablecoin), 10 ether);
    }

    function test_lockInvalidAmount() external {
      vm.prank(address(rollup));
      vm.expectRevert();
      staking.lock(address(stablecoin), 0);
    }

    function test_lockInvalidAsset(address _asset) external {
      vm.assume((_asset != address(stablecoin) && _asset != address(protocolToken)));
      vm.startPrank(address(rollup));
      vm.expectRevert();
      staking.lock(address(_asset), 10 ether);
    }

    function test_lockInsufficientCollateral() external {
      vm.prank(alice);
      staking.stake(address(stablecoin), 10 ether, tranches[0]);
      vm.prank(address(rollup));
      vm.expectRevert(abi.encodeWithSelector(INSUFFICIENT_COLLATERAL.selector, 20 ether, 10 ether));
      staking.lock(address(stablecoin), 20 ether);
    }

    function test_unlock() external {
      uint256 lockId = Id.unwrap(staking.currentLockId());
      uint256[] memory lockIds = new uint256[](1);
      lockIds[0] = lockId;
      vm.prank(alice);
      staking.stake(address(stablecoin), 10 ether, tranches[1]);

      uint256 lockBlockNumber = block.number;
      vm.prank(address(rollup));
      vm.expectEmit(true, true, true, true);
      emit Locked(address(stablecoin), 10 ether, 0);
      staking.lock(address(stablecoin), 10 ether);

      // we only need to wait fraudPeriod() here because the rollup is calling
      // the lock function when it receives a state root
      vm.roll(block.number + manager.fraudPeriod() + 1);

      // this is a bit trickier. in order to ensure that this is not an
      // integration test, we have to mock the storage in the rollup contract
      // so that we can control the data returned when we query for the lock
      vm.mockCall(
          address(rollup),
          abi.encodeWithSelector(Rollup.isFraudulentLockId.selector, lockId),
          abi.encode(false)
      );
      vm.mockCall(
          address(rollup),
          abi.encodeWithSelector(Rollup.isConfirmedLockId.selector, lockId),
          abi.encode(true)
      );

      vm.expectEmit(true, true, true, true);
      emit Unlocked(lockIds);
      staking.unlock(lockIds);

      (uint256 _amountLocked, uint256 _totalAmountStaked, uint256 _blockNumber, address _asset)
        = staking.locks(lockId);

      assertEq(_amountLocked, 0);
      assertEq(_totalAmountStaked, 10 ether);
      assertEq(_blockNumber, lockBlockNumber);
      assertEq(_asset, address(stablecoin));
      assertEq(Id.unwrap(staking.nextIdToUnlock()), lockIds[0] + 1);

      vm.clearMockedCalls();
    }

    function test_unlockMultiple() external {
      uint256[] memory lockIds = new uint256[](3);
      lockIds[0] = Id.unwrap(staking.currentLockId());
      lockIds[1] = Id.unwrap(staking.currentLockId()) + 1;
      lockIds[2] = Id.unwrap(staking.currentLockId()) + 2;
      vm.startPrank(alice);
      staking.stake(address(stablecoin), 10 ether, tranches[0]);
      staking.stake(address(stablecoin), 10 ether, tranches[1]);
      staking.stake(address(stablecoin), 10 ether, tranches[2]);
      vm.stopPrank();

      vm.startPrank(address(rollup));
      staking.lock(address(stablecoin), 5 ether);
      staking.lock(address(stablecoin), 10 ether);
      staking.lock(address(stablecoin), 15 ether);
      vm.stopPrank();

      vm.roll(block.number + manager.fraudPeriod() + 1);

      for (uint256 i = 0; i < lockIds.length; i++) {
        vm.mockCall(
          address(rollup),
          abi.encodeWithSelector(Rollup.isFraudulentLockId.selector, lockIds[i]),
          abi.encode(false)
        );
        vm.mockCall(
          address(rollup),
          abi.encodeWithSelector(Rollup.isConfirmedLockId.selector, lockIds[i]),
          abi.encode(true)
        );
      }

      vm.expectEmit(true, true, true, true);
      emit Unlocked(lockIds);
      staking.unlock(lockIds);

      assertEq(Id.unwrap(staking.nextIdToUnlock()), lockIds[2] + 1);
      vm.clearMockedCalls();
    }

    function test_unlockTooEarly() external {
      uint256 lockId = Id.unwrap(staking.currentLockId());
      uint256[] memory lockIds = new uint256[](1);
      lockIds[0] = lockId;
      vm.prank(alice);
      staking.stake(address(stablecoin), 10 ether, tranches[0]);

      vm.prank(address(rollup));
      staking.lock(address(stablecoin), 10 ether);

      vm.mockCall(
        address(rollup),
        abi.encodeWithSelector(Rollup.isFraudulentLockId.selector, lockId),
        abi.encode(false)
      );
      vm.mockCall(
        address(rollup),
        abi.encodeWithSelector(Rollup.isConfirmedLockId.selector, lockId),
        abi.encode(true)
      );

      vm.roll(block.number + manager.fraudPeriod() - 1);
      vm.expectRevert();
      staking.unlock(lockIds);
    }

    function test_unlockOutOfSequence() external {
      uint256 lockId = Id.unwrap(staking.nextIdToUnlock()) + 1;
      uint256[] memory lockIds = new uint256[](1);
      lockIds[0] = lockId;

      // technically don't even need to stake to test this condition
      vm.expectRevert();
      staking.unlock(lockIds);
    }

    function test_unlockFraudulentRoot() external {
      uint256 lockId = Id.unwrap(staking.nextIdToUnlock());
      uint256[] memory lockIds = new uint256[](1);
      lockIds[0] = lockId;

      // again we don't need to stake
      vm.mockCall(
        address(rollup),
        abi.encodeWithSelector(Rollup.isFraudulentLockId.selector, lockId),
        abi.encode(true)
      );

      vm.expectRevert();
      staking.unlock(lockIds);
    }

    function test_unlockUnconfirimedRoot() external {
      uint256 lockId = Id.unwrap(staking.nextIdToUnlock());
      uint256[] memory lockIds = new uint256[](1);
      lockIds[0] = lockId;

      // again we don't need to stake
      vm.mockCall(
          address(rollup),
          abi.encodeWithSelector(Rollup.isFraudulentLockId.selector, lockId),
          abi.encode(false)
      );
      vm.mockCall(
          address(rollup),
          abi.encodeWithSelector(Rollup.isConfirmedLockId.selector, lockId),
          abi.encode(false)
      );

      vm.expectRevert();
      staking.unlock(lockIds);
    }

    function test_reward(uint256 _lockId, uint256 _chainId, address _asset, uint256 _amount) external {
      vm.prank(address(rollup));
      vm.expectEmit(true, true, true, true);
      emit RewardAdded(_lockId, _chainId, _asset, _amount);
      staking.reward(_lockId, _chainId, _asset, _amount);
    }

    function test_rewardUnauthorised(address _caller) external {
      vm.assume(_caller != address(rollup));
      vm.prank(_caller);
      vm.expectRevert();
      staking.reward(0, 1, address(stablecoin), 10 ether);
    }

    function test_payInsurance(uint256 _chainId, address _asset, uint256 _amount) external {
      vm.prank(address(rollup));
      vm.expectEmit(true, true, true, true);
      emit InsurancePaid(_chainId, _asset, _amount);
      staking.payInsurance(_chainId, _asset, _amount);
      assertEq(staking.insuranceFees(_chainId, _asset), _amount);
    }

    function test_payInsuranceUnauthorised(address _caller) external {
      vm.assume(_caller != address(rollup));
      vm.prank(_caller);
      vm.expectRevert();
      staking.payInsurance(1, address(stablecoin), 10 ether);
    }

    function test_claimInsuranceFees(address _asset, uint256 _amount) external {
      vm.assume(_amount != 0);
      vm.deal(manager.admin(), 10 ether); // LZ fee payments
      address[] memory assets = new address[](1);
      assets[0] = _asset;

      vm.prank(address(rollup));
      staking.payInsurance(1, _asset, _amount);
      assertEq(staking.insuranceFees(1, _asset), _amount);

      vm.prank(manager.admin());
      staking.claimInsuranceFee{value: 0.5 ether}(1, assets, bytes(""));

      assertEq(staking.insuranceFees(1, _asset), 0);
    }

    function test_claimInsuranceFees() external {
      vm.deal(manager.admin(), 10 ether); // LZ fee payments
      address[] memory assets = new address[](2);
      assets[0] = address(stablecoin);
      assets[1] = address(protocolToken);

      vm.startPrank(address(rollup));
      staking.payInsurance(1, address(stablecoin), 10 ether);
      staking.payInsurance(1, address(protocolToken), 10 ether);
      assertEq(staking.insuranceFees(1, address(stablecoin)), 10 ether);
      assertEq(staking.insuranceFees(1, address(protocolToken)), 10 ether);
      vm.stopPrank();

      vm.prank(manager.admin());
      staking.claimInsuranceFee{value: 0.5 ether}(1, assets, bytes(""));

      assertEq(staking.insuranceFees(1, address(stablecoin)), 0);
      assertEq(staking.insuranceFees(1, address(protocolToken)), 0);
    }

    // function test_claimInsuranceFeesMultiple(address[] memory _assets, uint256[] memory _amounts) external {
    //   vm.assume(_assets.length == _amounts.length);
    //   vm.deal(manager.admin(), 10 ether); // LZ fee payments

    //   vm.startPrank(address(rollup));
    //   for (uint256 i = 0; i < _assets.length; i++) {
    //     vm.assume(_amounts[i] != 0);
    //     for (uint256 j = 0; j < _assets.length; j++) {
    //       vm.assume(_assets[i] != _assets[j]);
    //     }
    //     // we also need to assume that there are no duplicate _assets
    //     staking.payInsurance(1, _assets[i], _amounts[i]);
    //     assertEq(staking.insuranceFees(1, _assets[i]), _amounts[i]);
    //   }
    //   vm.stopPrank();

    //   vm.prank(manager.admin());
    //   staking.claimInsuranceFee{value: 0.5 ether}(1, _assets, );
    // }

    function test_claimInsuranceFeesNothingToClaim() external {
      vm.deal(manager.admin(), 10 ether);

      address[] memory assets = new address[](1);
      assets[0] = address(stablecoin);

      assertEq(staking.insuranceFees(1, address(stablecoin)), 0);

      vm.prank(manager.admin());
      vm.expectRevert();
      staking.claimInsuranceFee{value: 0.5 ether}(1, assets, bytes(""));
    }

    function test_claimInsuranceFeesUnauthorised(address _caller) external {
      vm.assume(_caller != manager.admin());
      vm.deal(_caller, 10 ether); // LZ fee payments

      address[] memory assets = new address[](1);
      assets[0] = address(stablecoin);

      vm.prank(address(rollup));
      staking.payInsurance(1, address(stablecoin), 10 ether);

      vm.prank(_caller);
      vm.expectRevert();
      staking.claimInsuranceFee{value: 0.5 ether}(1, assets, bytes(""));
    }

    function test_claimInsuranceFeesFundNotSet() external {
      vm.deal(manager.admin(), 10 ether);
      address[] memory assets = new address[](1);
      assets[0] = address(stablecoin);

      vm.mockCall(
        address(manager),
        abi.encodeWithSelector(manager.insuranceFund.selector),
        abi.encode(address(0))
      );

      vm.prank(manager.admin());
      vm.expectRevert();
      staking.claimInsuranceFee{value: 0.5 ether}(1, assets, bytes(""));
      vm.clearMockedCalls();
    }

    function test_claim() external {
      uint256[] memory locks = new uint256[](1);
      uint256[] memory deposits = new uint256[](1);
      uint256 rewardChainId = 1;
      address[] memory rewardAssets = new address[](1);

      uint256 lockId = Id.unwrap(staking.currentLockId());
      uint256 depositId = Id.unwrap(staking.currentDepositId());

      locks[0] = lockId;
      deposits[0] = depositId;
      rewardAssets[0] = address(stablecoin);

      // stake and lock all funds
      vm.prank(alice);
      staking.stake(address(stablecoin), 10 ether, tranches[0]);
      vm.roll(block.number + 1);
      vm.prank(address(rollup));
      staking.lock(address(stablecoin), 10 ether);

      // reward funds to the lock id
      vm.prank(address(rollup));
      staking.reward(lockId, rewardChainId, address(stablecoin), 10 ether);

      // roll to the end of the tranch plus the fraud period
      vm.roll(tranches[0] + manager.fraudPeriod() + 1);

      Staking.ClaimParams memory claim = Staking.ClaimParams(locks, deposits, rewardChainId, rewardAssets);

      // claim the reward
      vm.prank(alice);
      staking.claim{value: 0.5 ether}(claim);
    }

    function test_claimUnauthorised() external {
      uint256[] memory locks = new uint256[](1);
      uint256[] memory deposits = new uint256[](1);
      uint256 rewardChainId = 1;
      address[] memory rewardAssets = new address[](1);

      uint256 lockId = Id.unwrap(staking.currentLockId());
      uint256 depositId = Id.unwrap(staking.currentDepositId());

      locks[0] = lockId;
      deposits[0] = depositId;
      rewardAssets[0] = address(stablecoin);

      vm.prank(alice);
      staking.stake(address(stablecoin), 10 ether, tranches[0]);
      vm.roll(block.number + 1);
      vm.prank(address(rollup));
      staking.lock(address(stablecoin), 10 ether);

      vm.prank(address(rollup));
      staking.reward(lockId, rewardChainId, address(stablecoin), 10 ether);

      vm.roll(tranches[0] + manager.fraudPeriod() + 1);

      Staking.ClaimParams memory claim = Staking.ClaimParams(locks, deposits, rewardChainId, rewardAssets);

      vm.prank(bob);
      vm.expectRevert();
      staking.claim{value: 0.5 ether}(claim);
    }

    function test_claimNothing() external {
      uint256[] memory locks = new uint256[](1);
      uint256[] memory deposits = new uint256[](1);
      uint256 rewardChainId = 1;
      address[] memory rewardAssets = new address[](1);

      uint256 lockId = Id.unwrap(staking.currentLockId());
      uint256 depositId = Id.unwrap(staking.currentDepositId());

      locks[0] = lockId;
      deposits[0] = depositId;
      rewardAssets[0] = address(stablecoin);

      vm.prank(alice);
      staking.stake(address(stablecoin), 10 ether, tranches[0]);
      vm.roll(block.number + 1);
      vm.prank(address(rollup));
      staking.lock(address(stablecoin), 10 ether);

      Staking.ClaimParams memory claim = Staking.ClaimParams(locks, deposits, rewardChainId, rewardAssets);

      // no rewards issued, nothing to claim
      vm.prank(alice);
      vm.expectRevert();
      staking.claim{value: 0.5 ether}(claim);
    }

    function test_getActiveTranches() external {
      // TODO
    }

    function test_getAvailableCollateral() external {
      assertEq(staking.getAvailableCollateral(address(stablecoin)), 0);
      vm.startPrank(alice);
      staking.stake(address(stablecoin), 10 ether, tranches[0]);
      assertEq(staking.getAvailableCollateral(address(stablecoin)), 10 ether);
      staking.stake(address(stablecoin), 10 ether, tranches[0]);
      staking.stake(address(stablecoin), 10 ether, tranches[1]);
      staking.stake(address(stablecoin), 10 ether, tranches[2]);
      assertEq(staking.getAvailableCollateral(address(stablecoin)), 40 ether);
      vm.stopPrank();
    }

    function test_getAvailableCollateralInvalidAsset(address _asset) external {
      vm.assume(_asset != address(stablecoin) && _asset != address(protocolToken));
      vm.expectRevert();
      staking.getAvailableCollateral(_asset);
    }

    function test_getUserDepositIds() external {
      uint256[] memory deposits = new uint256[](4);
      uint256 currentDepositId = Id.unwrap(staking.currentDepositId());

      vm.startPrank(alice);
      staking.stake(address(stablecoin), 10 ether, tranches[0]);
      staking.stake(address(stablecoin), 10 ether, tranches[0]);
      staking.stake(address(stablecoin), 10 ether, tranches[1]);
      staking.stake(address(stablecoin), 10 ether, tranches[2]);
      vm.stopPrank();

      deposits[0] = currentDepositId;
      deposits[1] = currentDepositId + 1;
      deposits[2] = currentDepositId + 2;
      deposits[3] = currentDepositId + 3;

      assertEq(staking.getUserDepositIds(alice), deposits);
    }

    function test_getUserDepositRecords() external {
      Staking.DepositRecord[] memory r = new Staking.DepositRecord[](4);

      vm.startPrank(alice);
      staking.stake(address(stablecoin), 10 ether, tranches[0]);
      staking.stake(address(stablecoin), 10 ether, tranches[0]);
      staking.stake(address(stablecoin), 10 ether, tranches[1]);
      staking.stake(address(stablecoin), 10 ether, tranches[2]);
      vm.stopPrank();

      r[0] = Staking.DepositRecord(alice, address(stablecoin), 10 ether, block.number, tranches[0], 0);
      r[1] = Staking.DepositRecord(alice, address(stablecoin), 10 ether, block.number, tranches[0], 0);
      r[2] = Staking.DepositRecord(alice, address(stablecoin), 10 ether, block.number, tranches[1], 0);
      r[3] = Staking.DepositRecord(alice, address(stablecoin), 10 ether, block.number, tranches[2], 0);

      Staking.DepositRecord[] memory d = staking.getUserDepositRecords(alice);

      // this is a bit grotesque
      for (uint256 i = 0; i < r.length; i++) {
        assertEq(
          abi.encodePacked(d[i].staker, d[i].asset, d[i].amount, d[i].blockNumber, d[i].unlockTime, d[i].withdrawn),
          abi.encodePacked(r[i].staker, r[i].asset, r[i].amount, r[i].blockNumber, r[i].unlockTime, r[i].withdrawn)
        );
      }
    }

    function test_getAvailableDeposits() external {
      Staking.AvailableDeposit[] memory a = new Staking.AvailableDeposit[](4);
      uint256 currentDepositId = Id.unwrap(staking.currentDepositId());

      vm.startPrank(alice);
      staking.stake(address(stablecoin), 10 ether, tranches[0]);
      staking.stake(address(stablecoin), 10 ether, tranches[0]);
      staking.stake(address(stablecoin), 10 ether, tranches[1]);
      staking.stake(address(stablecoin), 10 ether, tranches[2]);
      vm.stopPrank();

      a[0] = Staking.AvailableDeposit(currentDepositId, address(stablecoin), 10 ether);
      a[1] = Staking.AvailableDeposit(currentDepositId + 1, address(stablecoin), 10 ether);
      a[2] = Staking.AvailableDeposit(currentDepositId + 2, address(stablecoin), 10 ether);
      a[3] = Staking.AvailableDeposit(currentDepositId + 3, address(stablecoin), 10 ether);

      // unlock first tranch
      vm.roll(tranches[0] + 1);
      Staking.AvailableDeposit[] memory d = staking.getAvailableDeposits(alice);

      for (uint256 i = 0; i < 2; i++) {
        assertEq(
          abi.encodePacked(d[i].id, d[i].asset, d[i].amount),
          abi.encodePacked(a[i].id, a[i].asset, a[i].amount)
        );
      }

      // second tranch
      vm.roll(tranches[1] + 1);
      d = staking.getAvailableDeposits(alice);

      for (uint256 i = 0; i < 3; i++) {
        assertEq(
          abi.encodePacked(d[i].id, d[i].asset, d[i].amount),
          abi.encodePacked(a[i].id, a[i].asset, a[i].amount)
        );
      }

      // finally
      vm.roll(tranches[2] + 1);
      d = staking.getAvailableDeposits(alice);

      for (uint256 i = 0; i < 4; i++) {
        assertEq(
          abi.encodePacked(d[i].id, d[i].asset, d[i].amount),
          abi.encodePacked(a[i].id, a[i].asset, a[i].amount)
        );
      }

    }

    function test_getAllLockRecords() external {
      Staking.LockRecord[] memory r = new Staking.LockRecord[](2);

      // stake, lock, stake, lock
      vm.prank(alice);
      staking.stake(address(stablecoin), 10 ether, tranches[0]);
      vm.roll(block.number + 1);

      r[0] = Staking.LockRecord(10 ether, 10 ether, block.number, address(stablecoin));
      vm.prank(address(rollup));
      staking.lock(address(stablecoin), 10 ether);

      vm.prank(alice);
      staking.stake(address(stablecoin), 10 ether, tranches[1]);
      vm.roll(block.number + 1);

      r[1] = Staking.LockRecord(10 ether, 20 ether, block.number, address(stablecoin));
      vm.prank(address(rollup));
      staking.lock(address(stablecoin), 10 ether);

      Staking.LockRecord[] memory l = staking.getAllLockRecords();

      for (uint256 i = 0; i < r.length; i++) {
        assertEq(
          abi.encodePacked(r[i].amountLocked, r[i].totalAmountStaked, r[i].blockNumber, r[i].asset),
          abi.encodePacked(l[i].amountLocked, l[i].totalAmountStaked, l[i].blockNumber, l[i].asset)
        );
      }
    }

    function test_getLockRecords() external {
      Staking.LockRecord[] memory r = new Staking.LockRecord[](5);

      vm.startPrank(alice);
      staking.stake(address(stablecoin), 50 ether, tranches[0]);
      vm.roll(block.number + 1);

      r[0] = Staking.LockRecord(10 ether, 50 ether, block.number, address(stablecoin));
      r[1] = Staking.LockRecord(10 ether, 50 ether, block.number, address(stablecoin));
      r[2] = Staking.LockRecord(10 ether, 50 ether, block.number, address(stablecoin));
      r[3] = Staking.LockRecord(10 ether, 50 ether, block.number, address(stablecoin));
      r[4] = Staking.LockRecord(10 ether, 50 ether, block.number, address(stablecoin));

      vm.startPrank(address(rollup));
      staking.lock(address(stablecoin), 10 ether);
      staking.lock(address(stablecoin), 10 ether);
      staking.lock(address(stablecoin), 10 ether);
      staking.lock(address(stablecoin), 10 ether);
      staking.lock(address(stablecoin), 10 ether);
      vm.stopPrank();

      Staking.LockRecord[] memory l = staking.getLockRecords(0, 2);
      for (uint256 i = 0; i < 2; i++) {
        assertEq(
          abi.encodePacked(r[i].amountLocked, r[i].totalAmountStaked, r[i].blockNumber, r[i].asset),
          abi.encodePacked(l[i].amountLocked, l[i].totalAmountStaked, l[i].blockNumber, l[i].asset)
        );
      }

      l = staking.getLockRecords(2, 5);
      for (uint256 i = 0; i < l.length; i++) {
        uint256 si = i + 2; // shift i
        assertEq(
          abi.encodePacked(r[si].amountLocked, r[si].totalAmountStaked, r[si].blockNumber, r[si].asset),
          abi.encodePacked(l[i].amountLocked, l[i].totalAmountStaked, l[i].blockNumber, l[i].asset)
        );
      }
    }

    function test_getLockRecordsInvalidRange() external {
      vm.expectRevert();
      staking.getLockRecords(0, 0); // from, to
      vm.expectRevert();
      staking.getLockRecords(1, 0); // from, to
      vm.expectRevert();
      staking.getLockRecords(1, 1); // from, to
    }

    function test_getUnlocked() external {
      vm.startPrank(alice);
      staking.stake(address(stablecoin), 10 ether, tranches[0]);
      staking.stake(address(protocolToken), 1000 ether, tranches[0]);
      staking.stake(address(stablecoin), 10 ether, tranches[1]);
      staking.stake(address(stablecoin), 10 ether, tranches[2]);
      staking.stake(address(protocolToken), 1000 ether, tranches[2]);
      vm.stopPrank();

      vm.roll(block.number + 1);
      (uint256 _unlockedStablecoin, uint256 _unlockedProtocolToken) = staking.getUnlocked(alice);
      assertEq(_unlockedStablecoin, 0);
      assertEq(_unlockedProtocolToken, 0);

      vm.roll(tranches[0] + 1);
      (_unlockedStablecoin, _unlockedProtocolToken) = staking.getUnlocked(alice);
      assertEq(_unlockedStablecoin, 10 ether);
      assertEq(_unlockedProtocolToken, 1000 ether);

      vm.roll(tranches[1] + 1);
      (_unlockedStablecoin, _unlockedProtocolToken) = staking.getUnlocked(alice);
      assertEq(_unlockedStablecoin, 20 ether);
      assertEq(_unlockedProtocolToken, 1000 ether);

      vm.roll(tranches[2] + 1);
      (_unlockedStablecoin, _unlockedProtocolToken) = staking.getUnlocked(alice);
      assertEq(_unlockedStablecoin, 30 ether);
      assertEq(_unlockedProtocolToken, 2000 ether);
    }

    function test_getAvailableToClaim() external {
      // stake as alice and bob
      vm.prank(alice);
      staking.stake(address(stablecoin), 10 ether, tranches[0]);
      vm.prank(bob);
      staking.stake(address(stablecoin), 10 ether, tranches[0]);

      vm.roll(block.number + 1);

      // lock their stakes
      vm.prank(address(rollup));
      staking.lock(address(stablecoin), 20 ether);

      // reward them
      vm.prank(address(rollup));
      staking.reward(0, 1, address(stablecoin), 10 ether);

      // check that we both have 5 ether worth of rewards
      assertEq(staking.getAvailableToClaim(alice, 1, address(stablecoin)), 5 ether);
      assertEq(staking.getAvailableToClaim(bob, 1, address(stablecoin)), 5 ether);
    }
}