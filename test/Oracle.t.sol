// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2023 TXA PTE. LTD.
pragma solidity 0.8.19;

import "./util/BaseTest.sol";

contract OracleTest is BaseTest {
    using IdLib for Id;

    function setUp() public override {
        super.setUp();
    }

    function test_grantReporter(address _reporter) external {
      vm.prank(admin);
      oracle.grantReporter(_reporter);
      assertEq(oracle.isReporter(_reporter), true);
    }

    function test_grantReporterUnauthorised(address _caller) external {
      vm.assume(_caller != admin);
      vm.prank(_caller);
      vm.expectRevert();
      oracle.grantReporter(address(1));
    }

    function test_revokeReporter(address _reporter) external {
      vm.startPrank(admin);
      oracle.grantReporter(_reporter);
      assertEq(oracle.isReporter(_reporter), true);
      oracle.revokeReporter(_reporter);
      assertEq(oracle.isReporter(_reporter), false);
    }

    function test_revokeReporterUnauthorised(address _caller) external {
      vm.assume(_caller != admin);
      vm.prank(_caller);
      vm.expectRevert();
      oracle.revokeReporter(address(1));
    }

    function test_initializePrice(uint256 _chainId, address _asset, uint256 _price) external {
      vm.mockCall(
        address(manager),
        abi.encodeWithSelector(manager.supportedAsset.selector, _chainId, _asset),
        abi.encode(18)
      );

      vm.prank(admin);
      oracle.initializePrice(_chainId, _asset, _price);

      assertEq(oracle.tokenPrecision(_chainId, _asset), 18); // from the mock
      assertEq(oracle.lastReport(_chainId, _asset), block.number);
      assertEq(oracle.latestPrice(_chainId, _asset), _price);
    }

    function test_initializePriceUnauthorised(address _caller) external {
      vm.assume(_caller != admin);
      vm.prank(_caller);
      vm.expectRevert();
      oracle.initializePrice(1, address(stablecoin), 10);
    }

    function test_initializePriceUnsupportedAsset() external {
      vm.mockCall(
        address(manager),
        abi.encodeWithSelector(manager.supportedAsset.selector, 1, address(stablecoin)),
        abi.encode(0)
      );

      vm.prank(admin);
      vm.expectRevert();
      oracle.initializePrice(1, address(stablecoin), 10);
    }

    function test_initializePriceTwice() external {
      // price was initialised in basetest
      vm.prank(admin);
      vm.expectRevert();
      oracle.initializePrice(1, address(0), 10);
    }

    function test_reportPrices() external {
      vm.roll(block.number + oracle.PRICE_COOLDOWN() + 1);
      Oracle.PriceReport[] memory prices = new Oracle.PriceReport[](2);
      prices[0] = Oracle.PriceReport(1, address(0), 1900.25e18);
      prices[1] = Oracle.PriceReport(31337, address(protocolToken), 0.31e18);
      vm.prank(admin);
      oracle.reportPrices(prices);
      assertEq(oracle.latestPrice(1, address(0)), 1900.25e18);
      assertEq(oracle.latestPrice(31337, address(protocolToken)), 0.31e18);
    }

    function test_report(uint8 _percentChange, bool _upOnly, bool _modulo) external {
      // asset initialised in base test
      vm.roll(block.number + oracle.PRICE_COOLDOWN() + 1);

      uint256 lastPrice = oracle.latestPrice(1, address(0));

      uint256 min = (lastPrice * 0.85e18) / 1e18;
      uint256 max = (lastPrice * 1.15e18) / 1e18;

      uint256 priceDelta = (_percentChange / 100) * lastPrice;
      uint256 newPrice;
      if (priceDelta > lastPrice && !_upOnly) {
        newPrice = 0;
      } else {
        newPrice = _upOnly ? lastPrice + priceDelta : lastPrice - priceDelta;
      }

      vm.startPrank(admin);
      if ((newPrice < min || newPrice > max) && !_modulo) {
        vm.expectRevert();
        oracle.report(1, address(0), newPrice, _modulo);
      } else if (_modulo && newPrice < min) {
        oracle.report(1, address(0), newPrice, _modulo);
        assertEq(oracle.lastReport(1, address(0)), block.number);
        assertEq(oracle.latestPrice(1, address(0)), min);
      } else if (_modulo && newPrice > max) {
        oracle.report(1, address(0), newPrice, _modulo);
        assertEq(oracle.lastReport(1, address(0)), block.number);
        assertEq(oracle.latestPrice(1, address(0)), max);
      } else {
        oracle.report(1, address(0), newPrice, _modulo);
        assertEq(oracle.lastReport(1, address(0)), block.number);
        assertEq(oracle.latestPrice(1, address(0)), newPrice);
      }
      vm.stopPrank();
    }

    function test_reportUnauthorised(address _caller) external {
      vm.assume(!oracle.isReporter(_caller));
      vm.prank(_caller);
      vm.expectRevert();
      oracle.report(1, address(0), 10, true);
    }

    function test_reportUnitialisedAsset(address _asset) external {
      vm.assume(oracle.lastReport(1, _asset) == 0);
      vm.assume(oracle.latestPrice(1, _asset) == 0);
      vm.prank(admin);
      vm.expectRevert();
      oracle.report(1, _asset, 10, true);
    }

    function test_reportBeforeCooldown() external {
      vm.roll(block.number + oracle.PRICE_COOLDOWN() + 1);
      vm.startPrank(admin);
      oracle.report(1, address(0), 2000e18, true);
      vm.roll(block.number + oracle.PRICE_COOLDOWN() - 1);
      vm.expectRevert();
      oracle.report(1, address(0), 2050e18, true);
      vm.stopPrank();
    }

    function test_reportPriceChangeTooLarge() external {
      vm.roll(block.number + oracle.PRICE_COOLDOWN() + 1);
      uint256 firstPrice = oracle.latestPrice(1, address(0));
      vm.startPrank(admin);
      vm.expectRevert();
      oracle.report(1, address(0), firstPrice * 2, false);
      vm.expectRevert();
      oracle.report(1, address(0), firstPrice / 2, false);
      vm.stopPrank();
    }

    function test_getStablecoinValue () external {
      uint256 amount = 2000e18;
      uint256 price = oracle.latestPrice(1, address(0));
      uint256 precision = oracle.tokenPrecision(1, address(0));

      // since reports always have 18 points of precision, we hardcode this
      uint256 coins = (amount * price) / 1e18;

      if (precision > 6) {
        coins = (coins / (10 ** (precision - 6)));
      } else if (precision < 6) {
        coins = (coins * (10 ** (6 - precision)));
      }

      uint256 value = oracle.getStablecoinValue(1, address(0), amount);
      assertEq(value, coins);
    }

    function test_getStablecoinValueCustomToken(uint8 _precision) external {
      vm.assume(_precision != 0); // not supported
      vm.assume(_precision < 24); // otherwise we'll overflow

      vm.mockCall(
        address(manager),
        abi.encodeWithSelector(manager.supportedAsset.selector, 1, address(1)),
        abi.encode(_precision)
      );

      vm.prank(admin);
      oracle.initializePrice(1, address(1), 100e18);

      uint256 amount = 2000 * (10 ** _precision);
      uint256 price = oracle.latestPrice(1, address(1));
      uint256 coins = (amount * price) / 1e18;

      if (_precision > 6) {
        coins = (coins / (10 ** (_precision - 6)));
      } else if (_precision < 6) {
        coins = (coins * (10 ** (6 - _precision)));
      }

      uint256 value = oracle.getStablecoinValue(1, address(1), amount);
      assertEq(value, coins);
    }

    function test_getStablecoinValueAssetChain(uint256 _amount) external {
      // check basetest for these params
      uint256 _chainId = 1;
      address _asset = address(stablecoin);
      assertEq(oracle.getStablecoinValue(_chainId, _asset, _amount), _amount);
    }

    function test_getStablecoinValueExpiredPrice() external {
      vm.roll(block.number + oracle.PRICE_EXPIRY() + 1);
      vm.expectRevert();
      oracle.getStablecoinValue(1, address(0), 100e18);
    }

    function test_stablecoinToProtocol(uint8 _amount) external {
      uint256 amount = uint256(_amount) * (10 ** 18);

      vm.mockCall(
        address(manager),
        abi.encodeWithSelector(manager.supportedAsset.selector, 31337, address(protocolToken)),
        abi.encode(18)
      );

      uint256 _price = oracle.latestPrice(31337, address(protocolToken));
      uint256 expected = ((amount * (10 ** 18)) / _price) * (10 ** 12);
      assertEq(oracle.stablecoinToProtocol(amount), expected);
    }

    function test_stablecoinToProtocolPriceExpired() external {
      vm.roll(block.number + oracle.PRICE_EXPIRY() + 1);
      vm.expectRevert();
      oracle.stablecoinToProtocol(100e18);
    }

}
