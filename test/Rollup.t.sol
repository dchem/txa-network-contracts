// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2023 TXA PTE. LTD.
pragma solidity 0.8.19;

import "./util/BaseTest.sol";
import "forge-std/console.sol";

contract RollupTest is BaseTest {
    // using IdLib for Id;

    error CALLER_NOT_VALIDATOR();
    uint256 aliceAmount = 0.5 ether;

    function setUp() public override {
        super.setUp();

        // in our tests we want to ensure that there is some stake
        deal({ token: address(protocolToken), to: validator, give: 20_000 ether });
        deal({ token: address(stablecoin), to: validator, give: 500 ether });
        uint256[3] memory tranches = staking.getActiveTranches();
        console.log(tranches[0]);
        console.log(tranches[1]);
        console.log(tranches[2]);
        vm.startPrank(validator);
        protocolToken.approve(manager.staking(), 20_000 ether);
        stablecoin.approve(manager.staking(), 500 ether);
        staking.stake(address(stablecoin), 500 ether, tranches[0]);
        staking.stake(address(protocolToken), 20_000 ether, tranches[0]);
        vm.stopPrank();
    }

    function defaultDeposits() internal returns (address[] memory who, uint256[] memory amounts) {
        address[] memory _who = new address[](3);
        _who[0] = alice;
        _who[1] = bob;
        _who[2] = bob;

        uint256[] memory _amounts = new uint256[](3);
        _amounts[0] = aliceAmount;
        _amounts[1] = 1 ether;
        _amounts[2] = 1.5 ether;

        return (_who, _amounts);
    }

    function mockOracleAndStaking(
        uint256 stablecoinValue,
        uint256 protocolValue,
        uint256 stablecoinAvailable,
        uint256 protocolTokenAvailable
    ) private {
        vm.mockCall(
            manager.oracle(),
            abi.encodeWithSelector(IOracle.getStablecoinValue.selector),
            abi.encode(stablecoinValue)
        );
        vm.mockCall(
            manager.oracle(),
            abi.encodeWithSelector(IOracle.stablecoinToProtocol.selector),
            abi.encode(protocolValue)
        );
        vm.mockCall(
            manager.staking(),
            abi.encodeWithSelector(IStaking.getAvailableCollateral.selector),
            abi.encode(stablecoinAvailable)
        );
        vm.mockCall(
            manager.staking(),
            abi.encodeWithSelector(IStaking.getAvailableCollateral.selector),
            abi.encode(protocolTokenAvailable)
        );
    }

    function makeDepositsNativeAsset(
        address[] memory who,
        uint256[] memory amount
    )
        internal
        returns (StateUpdateLibrary.Deposit[] memory)
    {
        StateUpdateLibrary.Deposit[] memory deposits = new StateUpdateLibrary.Deposit[](who.length);

        for (uint256 i; i < who.length; i++) {
            vm.prank(who[i]);
            portal.depositNativeAsset{ value: amount[i] }();
            deposits[i] = StateUpdateLibrary.Deposit(
                who[i], address(0), participatingInterface, amount[i], Id.wrap(i), Id.wrap(chainId)
            );
        }

        return deposits;
    }

    // todo: check the lockId
    function test_proposeStateRoot() external {
        // Obtain the current epoch and its corresponding proposed state root
        Id currentEpoch = rollup.epoch();

        // Assuming currentEpoch is of type Id and you want to assert it's 1
        assertEq(Id.unwrap(currentEpoch), 1);

        bytes32 lastProposedStateRoot = rollup.proposedStateRoot(currentEpoch);

        // Assuming lastProposedStateRoot should be the zero bytes32 value
        bytes32 zeroBytes32;
        assertEq(lastProposedStateRoot, zeroBytes32);

        // Create a new state root for testing
        bytes32 newStateRoot = keccak256(abi.encodePacked("new state root"));

        // Ensure the caller is a validator
        vm.prank(validator);

        // Call the proposeStateRoot function
        rollup.proposeStateRoot(lastProposedStateRoot, newStateRoot);

        // Check that state root is updated correctly for the new epoch
        assertEq(rollup.proposedStateRoot(currentEpoch), newStateRoot);

        // Check that proposalBlock is updated correctly for the new epoch and state root
        assertEq(rollup.proposalBlock(currentEpoch, newStateRoot), block.number);

        // Increment currentEpoch using IdLib's increment function
        Id nextEpoch = IdLib.increment(currentEpoch);

        // Check epoch increment
        assertEq(Id.unwrap(rollup.epoch()), Id.unwrap(nextEpoch));
    }

    function test_replaceStateRoot_should_replace_it() external {
        // Setup
        vm.prank(validator);

        bytes32 initialStateRoot = keccak256(abi.encodePacked("initial state root"));
        Id initialEpoch = Id.wrap(1); // 1 is the initial epoch
        rollup.proposeStateRoot("", initialStateRoot);

        // Act
        bytes32 newStateRoot = keccak256(abi.encodePacked("new state root"));
        vm.prank(validator);
        rollup.replaceStateRoot(newStateRoot, initialEpoch);

        // Assert
        assertEq(rollup.proposedStateRoot(initialEpoch), newStateRoot);
        assertEq(rollup.proposalBlock(initialEpoch, newStateRoot), block.number);
    }

    function test_markFraudulent() public {
        // Setup
        Id initialEpoch = rollup.epoch();
        vm.prank(validator);

        bytes32 stateRoot = keccak256(abi.encodePacked("initial state root"));
        rollup.proposeStateRoot("", stateRoot);

        // Act
        vm.prank(fraudEngine);
        rollup.markFraudulent(Id.unwrap(initialEpoch));

        // Assert
        assertTrue(rollup.fraudulent(initialEpoch, stateRoot));
    }

    function test_confirmStateRoot_NotValidator() public {
        // Arrange
        address nonValidator = address(2);
        vm.prank(nonValidator);

        // Act & Assert
        vm.expectRevert(CALLER_NOT_VALIDATOR.selector);
        rollup.confirmStateRoot();
    }

    function test_confirmStateRoot_EmptyStateRoot() public {
        // Arrange
        vm.prank(validator);

        // Act & Assert
        vm.expectRevert("Trying to confirm an empty state root");
        rollup.confirmStateRoot();
    }

    function test_confirmStateRoot_FraudPeriodNotPassed() public {
        // Setup
        vm.startPrank(validator);

        bytes32 initialStateRoot = keccak256(abi.encodePacked("initial state root"));
        rollup.proposeStateRoot("", initialStateRoot);

        // Assert
        vm.expectRevert("Proposed state root has not passed fraud period");
        rollup.confirmStateRoot();
        vm.stopPrank();
    }

    function test_confirmStateRoot_FraudulentStateRoot() public {
        // Setup
        Id initialEpoch = rollup.epoch();
        vm.prank(validator);

        bytes32 stateRoot = keccak256(abi.encodePacked("initial state root"));
        rollup.proposeStateRoot("", stateRoot);

        vm.prank(fraudEngine);
        rollup.markFraudulent(Id.unwrap(initialEpoch));

        // Simulate passage of time
        vm.roll(block.number + manager.fraudPeriod());

        // Act & Assert
        vm.prank(validator);
        vm.expectRevert("Trying to confirm a fraudulent state root");
        rollup.confirmStateRoot();
    }

    function test_confirmStateRoot_Success() public {
        // Setup
        vm.prank(validator);

        bytes32 stateRoot = keccak256(abi.encodePacked("initial state root"));
        rollup.proposeStateRoot("", stateRoot);

        // Simulate passage of time
        vm.roll(block.number + manager.fraudPeriod());

        // Act & Assert
        vm.prank(validator);
        rollup.confirmStateRoot();
    }

    function test_prepareSettlement_sufficientCollateral() public {
        // Arrange
        bytes32 testStateRoot = keccak256(abi.encodePacked("test state root"));

        // Create settlement request object
        StateUpdateLibrary.StateUpdate memory settlementAck = settlementStateUpdate(
            address(0),
            address(0),
            Id.wrap(0),
            Id.wrap(0),
            0,
            0
        );

        StateUpdateLibrary.SignedStateUpdate memory signedUpdate = signStateUpdate(settlementAck);

        // Set the values in the signedUpdate struct as needed

        // Mocking Oracle and Staking responses for sufficient collateral
        uint256 stablecoinValue = 1000; // example value
        uint256 protocolValue = 150; // example value
        uint256 stablecoinAvailable = 2000; // higher than stablecoinValue
        uint256 protocolTokenAvailable = 2000; // higher than protocolValue

        mockOracleAndStaking(stablecoinValue, protocolValue, stablecoinAvailable, protocolTokenAvailable);

        // Act
        rollup.testPrepareSettlement(testStateRoot, signedUpdate);

        // Assert
        assertEq(rollup.confirmedStateRoot(rollup.epoch()), bytes32(0));
    }

    function test_prepareSettlement_insufficientCollateral() public {
        // Arrange
        bytes32 testStateRoot = keccak256(abi.encodePacked("test state root"));

        // Create settlement request object
        StateUpdateLibrary.StateUpdate memory settlementAck = settlementStateUpdate(
            address(0),
            address(0),
            Id.wrap(0),
            Id.wrap(0),
            0,
            0
        );

        StateUpdateLibrary.SignedStateUpdate memory signedUpdate = signStateUpdate(settlementAck);

        // Set the values in the signedUpdate struct as needed

        // Mocking Oracle and Staking responses for sufficient collateral
        uint256 stablecoinValue = 3000; // example value
        uint256 protocolValue = 3000; // example value
        uint256 stablecoinAvailable = 2000; // higher than stablecoinValue
        uint256 protocolTokenAvailable = 2000; // higher than protocolValue

        mockOracleAndStaking(stablecoinValue, protocolValue, stablecoinAvailable, protocolTokenAvailable);

        // Act
        rollup.testPrepareSettlement(testStateRoot, signedUpdate);

        // Assert
        assertEq(rollup.confirmedStateRoot(rollup.epoch()), testStateRoot);
    }

    function test_processSettlement() external {
        // Forge seems to run this entire test (including staking deposits in super) in a single block
        // so we need to move the block number forward by at least 1.
        vm.roll(block.number + 1);

        (address[] memory depositors, uint256[] memory amounts) = defaultDeposits();

        StateUpdateLibrary.Deposit[] memory deposits = makeDepositsNativeAsset(depositors, amounts);

        vm.prank(alice);
        portal.requestSettlement(address(0));
        StateUpdateLibrary.StateUpdate memory settlementAck =
            settlementStateUpdate(alice, address(0), portal.chainSequenceId(), Id.wrap(0), 0, aliceAmount);

        (bytes32 stateRoot, StateUpdateLibrary.SignedStateUpdate memory stateUpdate, bytes32[] memory proof) =
            settlementData(deposits, settlementAck);

        // Propose state root as validator
        vm.prank(validator);
        rollup.proposeStateRoot("", stateRoot);

        // Report settlement as the validator
        vm.prank(validator);
        Rollup.SettlementParams[] memory params = new Rollup.SettlementParams[](1);
        params[0] = Rollup.SettlementParams(stateUpdate, ID_ONE, proof);
        rollup.processSettlements{ value: 1 ether }(Id.wrap(chainId), params);
        // TODO: Not sure why the mock requires this to be honest
        hyperlaneMockMailboxDest.processNextInboundMessage();

        // Alice can now withdraw original deposit minus settlement fee
        (uint256 insuranceFee, uint256 stakerRewards) =
            IFeeManager(address(manager)).calculateSettlementFees(aliceAmount);
        vm.prank(alice);
        portal.withdraw({ _amount: aliceAmount - (insuranceFee + stakerRewards), _token: address(0) });

        // Staker should be able to claim rewards
        uint256[] memory lockId = new uint256[](2);
        lockId[0] = 1;
        lockId[1] = 2;
        uint256[] memory depositId = new uint256[](2);
        depositId[0] = 1;
        depositId[1] = 2;
        address[] memory rewardAsset = new address[](1);
        rewardAsset[0] = address(0);
        Staking.ClaimParams memory claimParams = Staking.ClaimParams(lockId, depositId, chainId, rewardAsset);

        uint256 claimAmount = staking.getAvailableToClaim(validator, chainId, address(0));

        if (claimAmount == 0) revert("Claim amount should not be 0");
        vm.startPrank(validator);
        staking.claim{ value: 1 ether }(claimParams);
        // TODO: Not sure why this is required
        hyperlaneMockMailboxDest.processNextInboundMessage();
        if (staking.getAvailableToClaim(validator, chainId, address(0)) != 0) {
            revert("Claim amount should be 0 after claiming");
        }

        portal.withdraw(claimAmount, rewardAsset[0]);

        // Staker should not be able to withdraw staked assets
        vm.expectRevert();
        staking.withdraw(depositId);
        vm.stopPrank();

        // Simulate passage of time
        vm.roll(block.number + manager.fraudPeriod());

        // should not be able to unlock stake until state root is confirmed
        lockId = new uint256[](3);
        lockId[0] = 0;
        lockId[1] = 1;
        lockId[2] = 2;
        vm.expectRevert();
        staking.unlock(lockId);

        // Confirm state root
        vm.prank(validator);
        rollup.confirmStateRoot();

        // unlocked deposits view function should be all 0s
        Staking.AvailableDeposit[] memory unlockedIds = staking.getAvailableDeposits(validator);
        for (uint256 i = 0; i < unlockedIds.length; i++) {
            if (unlockedIds[i].id != 0) revert();
        }
        // getUnlockedAmount view function should return 0
        (uint256 unlockedStablecoin, uint256 unlockedProtocol) = staking.getUnlocked(validator);
        if (unlockedStablecoin != 0) revert();
        if (unlockedProtocol != 0) revert();

        // Simulate passage of time to unlock time of deposit
        (, , , , uint256 unlockTime, ) = staking.deposits(depositId[0]);
        vm.roll(unlockTime);
        // Staker should not be able to withdraw collateral since it wasn't unlocked
        vm.prank(validator);
        // vm.expectRevert();
        staking.withdraw(depositId);

        // Unlock collateral
        staking.unlock(lockId);

        // Get insurance fund fee
        uint256 insuranceFundAmount = staking.insuranceFees(chainId, address(0));
        if (insuranceFundAmount <= 0) revert("Insurance fee should have been set aside");

        vm.prank(admin);
        staking.claimInsuranceFee{ value: 0.5 ether }(chainId, rewardAsset, bytes(""));
        // TODO: Not sure why this is required
        hyperlaneMockMailboxDest.processNextInboundMessage();

        vm.prank(validator);
        portal.withdraw(insuranceFundAmount, address(0));
    }

    function test_submitSettlement() external {
        // Forge seems to run this entire test (including staking deposits in super) in a single block
        // so we need to move the block number forward by at least 1.
        vm.roll(block.number + 1);

        (address[] memory depositors, uint256[] memory amounts) = defaultDeposits();

        StateUpdateLibrary.Deposit[] memory deposits = makeDepositsNativeAsset(depositors, amounts);

        vm.prank(alice);
        portal.requestSettlement(address(0));
        StateUpdateLibrary.StateUpdate memory settlementAck =
            settlementStateUpdate(alice, address(0), portal.chainSequenceId(), Id.wrap(0), 0, aliceAmount);

        (bytes32 stateRoot, StateUpdateLibrary.SignedStateUpdate memory stateUpdate, bytes32[] memory proof) =
            settlementData(deposits, settlementAck);

        vm.prank(validator);
        rollup.submitSettlement{ value: 0.5 ether }(stateRoot, stateUpdate, proof);
        // TODO: Not sure why this is required
        hyperlaneMockMailboxDest.processNextInboundMessage();

        // Staker should be able to claim rewards
        uint256[] memory lockId = new uint256[](2);
        lockId[0] = 0;
        lockId[1] = 1;
        uint256[] memory depositId = new uint256[](2);
        depositId[0] = 1;
        depositId[1] = 2;
        address[] memory rewardAsset = new address[](1);
        rewardAsset[0] = address(0);
        Staking.ClaimParams memory claimParams = Staking.ClaimParams(lockId, depositId, chainId, rewardAsset);

        uint256 claimAmount = staking.getAvailableToClaim(validator, chainId, address(0));
        if (claimAmount == 0) revert("Claim amount should not be 0");
        vm.startPrank(validator);
        staking.claim{ value: 1 ether }(claimParams);
        // TODO: Not sure why this is required
        hyperlaneMockMailboxDest.processNextInboundMessage();
        if (staking.getAvailableToClaim(validator, chainId, address(0)) != 0) {
            revert("Claim amount should be 0 after claiming");
        }
        portal.withdraw(claimAmount, rewardAsset[0]);

        // Staker should not be able to withdraw staked assets
        vm.expectRevert();
        staking.withdraw(depositId);
        vm.stopPrank();

        // Simulate passage of time
        vm.roll(block.number + manager.fraudPeriod());

        // should not be able to unlock stake until state root is confirmed
        vm.expectRevert();
        staking.unlock(lockId);

        // Confirm state root
        vm.prank(validator);
        rollup.confirmStateRoot();

        // unlocked deposits view function should be all 0s
        Staking.AvailableDeposit[] memory unlockedIds = staking.getAvailableDeposits(validator);
        for (uint256 i = 0; i < unlockedIds.length; i++) {
            if (unlockedIds[i].id != 0) revert();
        }
        // getUnlockedAmount view function should return 0
        (uint256 unlockedStablecoin, uint256 unlockedProtocol) = staking.getUnlocked(validator);
        if (unlockedStablecoin != 0) revert();
        if (unlockedProtocol != 0) revert();

        // Simulate passage of time to unlock time of deposit
        (, , , , uint256 unlockTime, ) = staking.deposits(depositId[0]);
        vm.roll(unlockTime);
        // Staker should not be able to withdraw collateral since it wasn't unlocked
        vm.prank(validator);
        // vm.expectRevert();
        staking.withdraw(depositId);

        // Unlock collateral
        staking.unlock(lockId);

        // none of the unlocked deposit IDs should be 0
        unlockedIds = staking.getAvailableDeposits(validator);
        for (uint256 i = 0; i < unlockedIds.length; i++) {
            if (unlockedIds[i].id == 0) revert();
        }
        // getUnlockedAmount view function should NOT return 0
        (unlockedStablecoin, unlockedProtocol) = staking.getUnlocked(validator);
        if (unlockedStablecoin == 0) revert();
        if (unlockedProtocol == 0) revert();

        // after withdrawing remaining funds, unlocked deposit IDs should show 0
        vm.prank(validator);
        staking.withdraw(depositId);

        unlockedIds = staking.getAvailableDeposits(validator);
        for (uint256 i = 0; i < unlockedIds.length; i++) {
            if (unlockedIds[i].id != 0) revert();
        }
        // getUnlockedAmount should be 0 again
        (unlockedStablecoin, unlockedProtocol) = staking.getUnlocked(validator);
        if (unlockedStablecoin != 0) revert();
        if (unlockedProtocol != 0) revert();
    }

    function test_submitSettlementCollateralFallback() external {
        // Move time forward so all collateral is expired
        vm.roll(block.number + 1_000_000);

        (address[] memory depositors, uint256[] memory amounts) = defaultDeposits();

        StateUpdateLibrary.Deposit[] memory deposits = makeDepositsNativeAsset(depositors, amounts);

        vm.prank(alice);
        portal.requestSettlement(address(0));
        StateUpdateLibrary.StateUpdate memory settlementAck =
            settlementStateUpdate(alice, address(0), portal.chainSequenceId(), Id.wrap(0), 0, aliceAmount);

        (bytes32 stateRoot, StateUpdateLibrary.SignedStateUpdate memory stateUpdate, bytes32[] memory proof) =
            settlementData(deposits, settlementAck);

        vm.prank(validator);
        rollup.submitSettlement{ value: 0.5 ether }(stateRoot, stateUpdate, proof);
        // TODO: Not sure why this is required
        hyperlaneMockMailboxDest.processNextInboundMessage();

        // Alice can now withdraw original deposit minus settlement fee
        (uint256 insuranceFee, uint256 stakerRewards) =
            IFeeManager(address(manager)).calculateSettlementFees(aliceAmount);

        vm.prank(alice);
        portal.withdraw({ _amount: aliceAmount - (insuranceFee + stakerRewards), _token: address(0) });
    }
}
