// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2023 TXA PTE. LTD.
pragma solidity 0.8.19;

import "./util/BaseTest.sol";

contract MockReceiver {
    // this contract exists so that we can test the failure to withdraw. since
    // the contract has no payable fallback function, sending ether to it should
    // cause a revert. during correct operation of the system this is an extreme
    // edge case involving using selfdestruct to get funds to a contract like
    // this one in order to deposit them... unlikely but worth testing anyway
}

contract PortalTest is BaseTest {
    using IdLib for Id;

    event SettlementRequested(address trader, address token, Id chainSequenceId);
    event SettlementProcessed(address trader, address token, uint256 amount);
    event RejectedDeposit(address trader, address asset, uint256 amount);
    event Withdraw(address wallet, uint256 amount, address token);
    event WithdrawRejectedDeposit(address wallet, uint256 amount, address token);

    error INSUFFICIENT_BALANCE_WITHDRAW();
    error TRANSFER_FAILED_WITHDRAW();

    function setUp() public override {
        super.setUp();
    }

    function test_depositNativeAsset() external {
        uint256 aliceBalanceBefore = alice.balance;
        uint256 portalBalanceBefore = address(portal).balance;
        Id chainSequenceIdBefore = portal.chainSequenceId();
        uint256 amount = 0.5 ether;

        StateUpdateLibrary.Deposit memory deposit = StateUpdateLibrary.Deposit(
            alice, address(0), participatingInterface, amount, chainSequenceIdBefore, Id.wrap(block.chainid)
        );
        bytes32 utxo = keccak256(abi.encode(deposit));

        vm.prank(alice);
        vm.expectEmit(true, true, true, true);
        emit DepositUtxo(alice, amount, address(0), participatingInterface, chainSequenceIdBefore, utxo);
        portal.depositNativeAsset{ value: amount }();

        uint256 aliceBalanceAfter = alice.balance;
        uint256 portalBalanceAfter = address(portal).balance;
        assertEq(aliceBalanceBefore - aliceBalanceAfter, amount);
        assertEq(portalBalanceAfter - portalBalanceBefore, amount);
        assertTrue(chainSequenceIdBefore.increment() == portal.chainSequenceId());
    }

    function test_depositNativeAssetUniqueness() external {
        uint256 amount = 0.5 ether;
        Id chainSequenceIdBefore = portal.chainSequenceId();
        StateUpdateLibrary.Deposit memory deposit = StateUpdateLibrary.Deposit(
            alice, address(0), participatingInterface, amount, chainSequenceIdBefore, Id.wrap(block.chainid)
        );
        vm.prank(alice);
        portal.depositNativeAsset{ value: amount }();

        // Second deposit should have different UTXO hash and chain sequence ID
        deposit = StateUpdateLibrary.Deposit(
            alice, address(0), participatingInterface, amount, chainSequenceIdBefore.increment(), Id.wrap(block.chainid)
        );
        bytes32 utxo = keccak256(abi.encode(deposit));
        vm.prank(alice);
        vm.expectEmit(true, true, true, true);
        emit DepositUtxo(alice, amount, address(0), participatingInterface, chainSequenceIdBefore.increment(), utxo);
        portal.depositNativeAsset{ value: amount }();
    }

    function test_depositNativeAssetBelowMinimum() external {
        vm.prank(alice);
        vm.expectRevert();
        portal.depositNativeAsset{value: 1}();
    }

    function test_depositToken() external {
        uint256 amount = 0.5 ether;
        deal({ token: address(token), to: alice, give: amount });
        uint256 aliceBalanceBefore = token.balanceOf(alice);
        uint256 portalBalanceBefore = token.balanceOf(address(portal));
        Id chainSequenceIdBefore = portal.chainSequenceId();

        StateUpdateLibrary.Deposit memory deposit = StateUpdateLibrary.Deposit(
            alice, address(token), participatingInterface, amount, chainSequenceIdBefore, Id.wrap(block.chainid)
        );
        bytes32 utxo = keccak256(abi.encode(deposit));

        vm.startPrank(alice);
        token.approve({ spender: address(portal), amount: amount });
        vm.expectEmit(true, true, true, true);
        emit DepositUtxo(alice, amount, address(token), participatingInterface, chainSequenceIdBefore, utxo);
        portal.depositToken({ _token: address(token), _amount: amount });
        vm.stopPrank();

        uint256 aliceBalanceAfter = token.balanceOf(alice);
        uint256 portalBalanceAfter = token.balanceOf(address(portal));
        assertEq(aliceBalanceBefore - aliceBalanceAfter, amount);
        assertEq(portalBalanceAfter - portalBalanceBefore, amount);
        assertTrue(chainSequenceIdBefore.increment() == portal.chainSequenceId());
    }

    function test_depositTokenBelowMinimum() external {
        vm.prank(alice);
        vm.expectRevert();
        portal.depositToken(address(token), 1);
    }

    function test_withdrawUnsettled() external {
        uint256 amount = 0.5 ether;
        vm.startPrank(alice);
        portal.depositNativeAsset{ value: amount }();
        deal({ token: address(token), to: alice, give: amount });
        token.approve({ spender: address(portal), amount: amount });
        portal.depositToken({ _token: address(token), _amount: amount });

        vm.expectRevert();
        portal.withdraw(1, address(0));
        vm.expectRevert();
        portal.withdraw(1, address(token));
    }

    function test_pauseDeposits() external {
        vm.prank(manager.admin());
        portal.pauseDeposits();

        uint256 amount = 0.5 ether;
        deal({ token: address(token), to: alice, give: amount });
        vm.startPrank(alice);

        vm.expectRevert();
        portal.depositNativeAsset{ value: amount }();

        token.approve({ spender: address(portal), amount: amount });
        vm.expectRevert();
        portal.depositToken({ _token: address(token), _amount: amount });

        vm.stopPrank();
    }

    function test_pauseDepositsUnauthorised() external {
        vm.expectRevert();
        portal.pauseDeposits();
    }

    function test_resumeDeposits() external {
        vm.prank(manager.admin());
        portal.resumeDeposits();
        this.test_depositToken();
    }

    function test_resumeDepositsUnauthorised() external {
        vm.expectRevert();
        portal.resumeDeposits();
    }

    function test_requestSettlement() external {
        vm.expectEmit(false, false, false, false);
        Id currentId = portal.chainSequenceId();
        emit SettlementRequested(alice, address(0), currentId);
        vm.prank(alice);
        portal.requestSettlement(address(0));

        uint256 nextId = Id.unwrap(currentId) + 1;
        assertEq(Id.unwrap(portal.chainSequenceId()), nextId);

        (address _trader, address _asset, address _participatingInterface, Id _chainSequenceId, Id _chainId) =
            portal.settlementRequests(currentId);

        assertEq(_trader, alice);
        assertEq(_asset, address(0));
        assertEq(_participatingInterface, participatingInterface);
        assertEq(Id.unwrap(_chainSequenceId), Id.unwrap(currentId));
        assertEq(Id.unwrap(_chainId), block.chainid);
    }

    function test_requestSettlementUnsupportedAsset() external {
        vm.prank(alice);
        vm.expectRevert();
        portal.requestSettlement(address(1));
    }

    function test_writeObligations() external {
        // this test is a bit weird, because we're not testing a scenario that
        // _should_ happen. we're testing that the writeObligations function
        // trusts the obligations sent to it, and will enact them regardless of
        // the state of the program. in a sense, this demonstrates that the
        // inputs to the rest of the program must be correct, and shines light
        // on the parts of the protocol which require trust (in this case we're
        // trusting that the message coming from hyperlane is correct).

        IPortal.Obligation[] memory obligations = new IPortal.Obligation[](1);
        obligations[0].recipient = alice;
        obligations[0].asset = address(0);
        obligations[0].amount = 1 ether;

        // bob deposits
        vm.prank(bob);
        portal.depositNativeAsset{ value: 1 ether }();

        // write obligations again, should have enough collateral now
        vm.prank(address(assetChainHyperlane));
        vm.expectEmit(false, false, false, false);
        emit SettlementProcessed(alice, address(0), 1 ether);
        portal.writeObligations(obligations);

        assertEq(portal.collateralized(address(0)), 0);
        assertEq(portal.settled(alice, address(0)), 1 ether);
        assertEq(portal.getAvailableBalance(alice, address(0)), 1 ether);
    }

    function test_writeObligationsMultiple() external {
        vm.prank(alice);
        portal.depositNativeAsset{ value: 10 ether }();

        IPortal.Obligation[] memory obligationsFirst = new IPortal.Obligation[](2);
        obligationsFirst[0].recipient = alice;
        obligationsFirst[0].asset = address(0);
        obligationsFirst[0].amount = 1 ether;

        obligationsFirst[1].recipient = bob;
        obligationsFirst[1].asset = address(0);
        obligationsFirst[1].amount = 1.5 ether;

        vm.prank(address(assetChainHyperlane));
        vm.expectEmit(false, false, false, false);
        emit SettlementProcessed(alice, address(0), 1 ether);
        vm.expectEmit(false, false, false, false);
        emit SettlementProcessed(bob, address(0), 1.5 ether);
        portal.writeObligations(obligationsFirst);

        assertEq(portal.collateralized(address(0)), 7.5 ether);
        assertEq(portal.settled(alice, address(0)), 1 ether);
        assertEq(portal.settled(bob, address(0)), 1.5 ether);

        IPortal.Obligation[] memory obligationsSecond = new IPortal.Obligation[](1);
        obligationsSecond[0].recipient = alice;
        obligationsSecond[0].asset = address(0);
        obligationsSecond[0].amount = 2.5 ether;

        vm.prank(address(assetChainHyperlane));
        vm.expectEmit(false, false, false, false);
        emit SettlementProcessed(alice, address(0), 2.5 ether);
        portal.writeObligations(obligationsSecond);

        assertEq(portal.collateralized(address(0)), 5.0 ether);
        assertEq(portal.settled(alice, address(0)), 3.5 ether);
        assertEq(portal.settled(bob, address(0)), 1.5 ether);
    }

    function test_writeObligationsInsufficientCollateral() external {
        IPortal.Obligation[] memory obligations = new IPortal.Obligation[](1);
        obligations[0].recipient = alice;
        obligations[0].asset = address(0);
        obligations[0].amount = 1 ether;

        vm.prank(address(assetChainHyperlane));
        vm.expectRevert();
        portal.writeObligations(obligations);
    }

    function test_writeObligationsUnauthorised() external {
        vm.prank(alice);
        portal.depositNativeAsset{ value: 10 ether }();

        IPortal.Obligation[] memory obligations = new IPortal.Obligation[](1);
        obligations[0].recipient = alice;
        obligations[0].asset = address(0);
        obligations[0].amount = 1 ether;

        vm.prank(alice);
        vm.expectRevert();
        portal.writeObligations(obligations);
    }

    function test_withdrawSettled() external {
        vm.prank(alice);
        portal.depositNativeAsset{ value: 10 ether }();

        IPortal.Obligation[] memory obligations = new IPortal.Obligation[](1);
        obligations[0].recipient = alice;
        obligations[0].asset = address(0);
        obligations[0].amount = 1 ether;

        vm.prank(address(assetChainHyperlane));
        portal.writeObligations(obligations);

        vm.prank(alice);
        vm.expectEmit(true, true, true, true);
        emit Withdraw(alice, 1 ether, address(0));
        portal.withdraw(1 ether, address(0));

        assertEq(portal.settled(alice, address(0)), 0);
    }

    function test_withdrawSettledToken() external {
        deal({ token: address(token), to: alice, give: 10 ether });
        vm.startPrank(alice);
        token.approve(address(portal), 10 ether);
        portal.depositToken(address(token), 10 ether);
        vm.stopPrank();

        IPortal.Obligation[] memory obligations = new IPortal.Obligation[](1);
        obligations[0].recipient = alice;
        obligations[0].asset = address(token);
        obligations[0].amount = 1 ether;

        vm.prank(address(assetChainHyperlane));
        portal.writeObligations(obligations);

        vm.prank(alice);
        vm.expectEmit(true, true, true, true);
        emit Withdraw(alice, 1 ether, address(token));
        portal.withdraw(1 ether, address(token));

        assertEq(portal.settled(alice, address(token)), 0);
    }

    function test_withdrawSettledInsufficientBalance() external {
        vm.prank(alice);
        portal.depositNativeAsset{ value: 10 ether }();

        IPortal.Obligation[] memory obligations = new IPortal.Obligation[](1);
        obligations[0].recipient = alice;
        obligations[0].asset = address(0);
        obligations[0].amount = 1 ether;

        vm.prank(address(assetChainHyperlane));
        portal.writeObligations(obligations);

        vm.prank(alice);
        vm.expectRevert(INSUFFICIENT_BALANCE_WITHDRAW.selector);
        portal.withdraw(2 ether, address(0));
    }

    function test_withdrawSettledTokenInsufficientBalance() external {
        deal({ token: address(token), to: alice, give: 10 ether });
        vm.startPrank(alice);
        token.approve(address(portal), 10 ether);
        portal.depositToken(address(token), 10 ether);
        vm.stopPrank();


        IPortal.Obligation[] memory obligations = new IPortal.Obligation[](1);
        obligations[0].recipient = alice;
        obligations[0].asset = address(token);
        obligations[0].amount = 1 ether;

        vm.prank(address(assetChainHyperlane));
        portal.writeObligations(obligations);

        vm.prank(alice);
        vm.expectRevert(INSUFFICIENT_BALANCE_WITHDRAW.selector);
        portal.withdraw(2 ether, address(0));
    }

    function test_withdrawSettledInvalid() external {
        address receiver = address(new MockReceiver());

        // mockreceiver as-is doesn't support depositing or withdrawing, but it
        // could theoretically
        vm.deal(receiver, 10 ether);
        vm.prank(receiver);
        portal.depositNativeAsset{ value: 10 ether }();

        IPortal.Obligation[] memory obligations = new IPortal.Obligation[](1);
        obligations[0].recipient = receiver;
        obligations[0].asset = address(0);
        obligations[0].amount = 1 ether;

        vm.prank(address(assetChainHyperlane));
        portal.writeObligations(obligations);

        vm.prank(receiver);
        vm.expectRevert(TRANSFER_FAILED_WITHDRAW.selector);
        portal.withdraw(1 ether, address(0));
    }

    function test_rejectDeposits() external {
        StateUpdateLibrary.Deposit memory deposit = StateUpdateLibrary.Deposit(
            alice, address(0), participatingInterface, 10 ether, portal.chainSequenceId(), Id.wrap(block.chainid)
        );
        bytes32 utxo = keccak256(abi.encode(deposit));

        vm.prank(alice);
        portal.depositNativeAsset{value: 10 ether}();

        bytes32[] memory depositHashes = new bytes32[](1);
        depositHashes[0] = utxo;

        // try to withdraw as alice, should fail
        vm.prank(alice);
        vm.expectRevert();
        portal.withdrawRejected(10 ether, address(0));

        // reject the deposit as lz
        vm.prank(address(assetChainHyperlane));
        portal.rejectDeposits(depositHashes);

        assertEq(portal.rejected(alice, address(0)), 10 ether);
    }

    function test_rejectDepositsToken() external {
        StateUpdateLibrary.Deposit memory deposit = StateUpdateLibrary.Deposit(
            alice, address(token), participatingInterface, 10 ether, portal.chainSequenceId(), Id.wrap(block.chainid)
        );
        bytes32 utxo = keccak256(abi.encode(deposit));

        deal({ token: address(token), to: alice, give: 10 ether });
        vm.startPrank(alice);
        token.approve(address(portal), 10 ether);
        portal.depositToken(address(token), 10 ether);
        vm.stopPrank();

        bytes32[] memory depositHashes = new bytes32[](1);
        depositHashes[0] = utxo;

        // try to withdraw as alice, should fail
        vm.prank(alice);
        vm.expectRevert();
        portal.withdrawRejected(10 ether, address(token));

        // reject the deposit as lz
        vm.prank(address(assetChainHyperlane));
        portal.rejectDeposits(depositHashes);

        assertEq(portal.rejected(alice, address(token)), 10 ether);
    }

    function test_rejectDepositsMultiple() external {

    }

    function test_rejectDepositsNonexistent() external {
        // construct a deposit
        StateUpdateLibrary.Deposit memory deposit = StateUpdateLibrary.Deposit(
            alice, address(0), participatingInterface, 5 ether, portal.chainSequenceId(), Id.wrap(block.chainid)
        );
        bytes32 utxo = keccak256(abi.encode(deposit));

        // make a different deposit (note amount does not match)
        vm.prank(alice);
        portal.depositNativeAsset{value: 10 ether}();

        bytes32[] memory depositHashes = new bytes32[](1);
        depositHashes[0] = utxo;

        // reject the deposit as lz, should fail because deposit does not exist
        vm.prank(address(assetChainHyperlane));
        vm.expectRevert();
        portal.rejectDeposits(depositHashes);
    }

    function test_rejectDepositsDuplicate() external {
        StateUpdateLibrary.Deposit memory deposit = StateUpdateLibrary.Deposit(
            alice, address(0), participatingInterface, 10 ether, portal.chainSequenceId(), Id.wrap(block.chainid)
        );
        bytes32 utxo = keccak256(abi.encode(deposit));

        vm.prank(alice);
        portal.depositNativeAsset{value: 10 ether}();

        bytes32[] memory depositHashes = new bytes32[](1);
        depositHashes[0] = utxo;

        vm.prank(address(assetChainHyperlane));
        portal.rejectDeposits(depositHashes);

        // reject the deposit a second time, should fail
        vm.prank(address(assetChainHyperlane));
        vm.expectRevert();
        portal.rejectDeposits(depositHashes);
    }

    function test_rejectDepositsInsufficientBalance() external {
        // this one is a bit more difficult to test. we have to make a deposit
        // to collateralise the contract, but then withdraw the deposit via some
        // other mechanism, before we can try to reject the deposit.
        StateUpdateLibrary.Deposit memory deposit = StateUpdateLibrary.Deposit(
            alice, address(0), participatingInterface, 10 ether, portal.chainSequenceId(), Id.wrap(block.chainid)
        );
        bytes32 utxo = keccak256(abi.encode(deposit));

        vm.prank(alice);
        portal.depositNativeAsset{value: 10 ether}();

        bytes32[] memory depositHashes = new bytes32[](1);
        depositHashes[0] = utxo;

        // now that we have deposited and the contract is collateralised to 10
        // ether, we should give someone else the obligation to withdraw some of
        // the ether, and then when we submit our rejection, there will not be
        // enough collateral
        IPortal.Obligation[] memory obligations = new IPortal.Obligation[](1);
        obligations[0].recipient = bob;
        obligations[0].asset = address(0);
        obligations[0].amount = 1 ether;

        vm.startPrank(address(assetChainHyperlane));
        portal.writeObligations(obligations);
        vm.expectRevert();
        portal.rejectDeposits(depositHashes);
        vm.stopPrank();
    }

    function test_rejectDepositsUnauthorised() external {
        StateUpdateLibrary.Deposit memory deposit = StateUpdateLibrary.Deposit(
            alice, address(0), participatingInterface, 10 ether, portal.chainSequenceId(), Id.wrap(block.chainid)
        );
        bytes32 utxo = keccak256(abi.encode(deposit));

        vm.prank(alice);
        portal.depositNativeAsset{value: 10 ether}();

        bytes32[] memory depositHashes = new bytes32[](1);
        depositHashes[0] = utxo;

        vm.prank(alice);
        vm.expectRevert();
        portal.rejectDeposits(depositHashes);
    }

    function test_withdrawRejected() external {
        StateUpdateLibrary.Deposit memory deposit = StateUpdateLibrary.Deposit(
            alice, address(0), participatingInterface, 10 ether, portal.chainSequenceId(), Id.wrap(block.chainid)
        );
        bytes32 utxo = keccak256(abi.encode(deposit));

        vm.prank(alice);
        portal.depositNativeAsset{value: 10 ether}();

        bytes32[] memory depositHashes = new bytes32[](1);
        depositHashes[0] = utxo;

        vm.prank(address(assetChainHyperlane));
        portal.rejectDeposits(depositHashes);

        vm.expectEmit(true, true, true, true);
        emit WithdrawRejectedDeposit(alice, 10 ether, address(0));
        vm.prank(alice);
        portal.withdrawRejected(10 ether, address(0));

        assertEq(portal.rejected(alice, address(0)), 0);
        assertEq(portal.collateralized(address(0)), 0);
        assertEq(portal.rejectedDeposits(utxo), true);
    }

    function test_withdrawRejectedToken() external {
        StateUpdateLibrary.Deposit memory deposit = StateUpdateLibrary.Deposit(
            alice, address(token), participatingInterface, 10 ether, portal.chainSequenceId(), Id.wrap(block.chainid)
        );
        bytes32 utxo = keccak256(abi.encode(deposit));

        deal({ token: address(token), to: alice, give: 10 ether });
        vm.startPrank(alice);
        token.approve(address(portal), 10 ether);
        portal.depositToken(address(token), 10 ether);
        vm.stopPrank();

        bytes32[] memory depositHashes = new bytes32[](1);
        depositHashes[0] = utxo;

        vm.prank(address(assetChainHyperlane));
        portal.rejectDeposits(depositHashes);

        vm.expectEmit(true, true, true, true);
        emit WithdrawRejectedDeposit(alice, 10 ether, address(token));
        vm.prank(alice);
        portal.withdrawRejected(10 ether, address(token));

        assertEq(portal.rejected(alice, address(token)), 0);
        assertEq(portal.collateralized(address(token)), 0);
        assertEq(portal.rejectedDeposits(utxo), true);
    }

    function test_withdrawRejectedInsufficientBalance() external {
        vm.prank(alice);
        vm.expectRevert(INSUFFICIENT_BALANCE_WITHDRAW.selector);
        portal.withdrawRejected(10 ether, address(0));
    }

    function test_withdrawRejectedInvalid() external {
        address receiver = address(new MockReceiver());

        StateUpdateLibrary.Deposit memory deposit = StateUpdateLibrary.Deposit(
            receiver, address(0), participatingInterface, 10 ether, portal.chainSequenceId(), Id.wrap(block.chainid)
        );
        bytes32 utxo = keccak256(abi.encode(deposit));

        vm.deal(receiver, 10 ether);
        vm.prank(receiver);
        portal.depositNativeAsset{value: 10 ether}();

        bytes32[] memory depositHashes = new bytes32[](1);
        depositHashes[0] = utxo;

        vm.prank(address(assetChainHyperlane));
        portal.rejectDeposits(depositHashes);

        vm.prank(receiver);
        vm.expectRevert(TRANSFER_FAILED_WITHDRAW.selector);
        portal.withdrawRejected(10 ether, address(0));
    }
}
