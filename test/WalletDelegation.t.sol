// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2023 TXA PTE. LTD.
pragma solidity 0.8.19;

import "./util/BaseTest.sol";

contract WalletDelegationTest is BaseTest {
    using IdLib for Id;

    enum DelegationState {
      NONE,
      APPROVED,
      REVOKED
    }

    event WalletApproved(address delegator, address delegatee, Id chainSequenceId);
    event WalletRevoked(address delegator, address delegatee, Id chainSequenceId);

    function setUp() public override {
        super.setUp();
    }

    function test_approve(address _delegatee) external {
      vm.prank(alice);
      vm.expectEmit(true, true, true, true);
      emit WalletApproved(alice, _delegatee, Id.wrap(1));
      walletDelegation.approve(_delegatee);
      assertEq(Id.unwrap(walletDelegation.approvals(alice, _delegatee)), 1);
    }

    function test_approveTwice(address _delegatee) external {
      vm.startPrank(alice);
      walletDelegation.approve(_delegatee);
      vm.expectRevert();
      walletDelegation.approve(_delegatee);
      vm.stopPrank();
    }

    function test_revoke(address _delegatee) external {
      vm.startPrank(alice);
      walletDelegation.approve(_delegatee); // increments the id
      vm.expectEmit(true, true, true, true);
      emit WalletRevoked(alice, _delegatee, Id.wrap(2));
      walletDelegation.revoke(_delegatee);
      vm.stopPrank();
      assertEq(Id.unwrap(walletDelegation.revokations(alice, _delegatee)), 2);
    }

    function test_revokeUnapproved(address _delegatee) external {
      vm.prank(alice);
      vm.expectRevert();
      walletDelegation.revoke(_delegatee);
    }

    function test_revokeTwice(address _delegatee) external {
      vm.startPrank(alice);
      walletDelegation.approve(_delegatee);
      walletDelegation.revoke(_delegatee);
      vm.expectRevert();
      walletDelegation.revoke(_delegatee);
      vm.stopPrank();
    }

}
