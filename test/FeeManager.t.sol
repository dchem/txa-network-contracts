// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2023 TXA PTE. LTD.
pragma solidity 0.8.19;

import "./util/BaseTest.sol";

contract FeeManagerTest is BaseTest {
    using IdLib for Id;

    event TradingFeesProposed(uint256 makerFee, uint256 takerFee);
    event TradingFeesUpdated(Id indexed feeSequenceId, uint256 makerFee, uint256 takerFee);

    function setUp() public override {
        super.setUp();
    }

    function test_calculateSettlementFees() external {
        (uint256 insuranceFee, uint256 stakerReward) = manager.calculateSettlementFees(10 ether);
        assertEq(insuranceFee, 0.005 ether);
        assertEq(stakerReward, 0.005 ether);
    }

    function test_calculateInsuranceFee() external {
        uint256 insuranceFee = manager.calculateInsuranceFee(10 ether);
        assertEq(insuranceFee, 5 ether);
    }

    function test_calculateStakingRewards() external {
        (uint256 stablePoolReward, uint256 protocolPoolReward) = manager.calculateStakingRewards(10 ether);
        assertEq(stablePoolReward, 8_696_000_000_000_000_000);
        assertEq(protocolPoolReward, 1_304_000_000_000_000_000);
        // for sanity
        uint256 total = 8_696_000_000_000_000_000 + 1_304_000_000_000_000_000;
        assertEq(total, 10 ether);
    }

    function test_proposeFeesUnauthorised() external {
        vm.expectRevert();
        manager.proposeFees(0, 0);
    }

    function test_proposeFeesAuthorised() external {
        vm.startPrank(participatingInterface);

        // too small
        vm.expectRevert();
        manager.proposeFees(1, 1);
        vm.expectRevert();
        manager.proposeFees(1, 0);
        vm.expectRevert();
        manager.proposeFees(0, 1);

        // too large
        vm.expectRevert();
        manager.proposeFees(1 ether, 1 ether);
        vm.expectRevert();
        manager.proposeFees(1 ether, 0);
        vm.expectRevert();
        manager.proposeFees(0, 1 ether);

        // we permit fees of zero
        vm.expectEmit(true, true, false, false);
        emit TradingFeesProposed(0, 0);
        manager.proposeFees(0, 0);

        vm.stopPrank();
    }

    function test_updateFeesUnauthorised() external {
        vm.startPrank(participatingInterface);
        manager.proposeFees(0, 0);
        vm.stopPrank();

        // move forward one day (the confirmation timeout)
        skip(86_400);

        vm.expectRevert();
        manager.updateFees();
    }

    function test_updateFees() external {
        vm.startPrank(participatingInterface);
        manager.proposeFees(0, 0);

        // should revert if we try to confirm too early
        vm.expectRevert();
        manager.updateFees();

        // move forward one day (the confirmation timeout)
        skip(86_400);

        vm.expectEmit(true, false, false, false);
        emit TradingFeesUpdated(Id.wrap(1), 0, 0);
        manager.updateFees();
        vm.stopPrank();
    }

    function test_updateFeesSequenceNumber() external {
        vm.startPrank(participatingInterface);

        manager.proposeFees(0, 0);
        skip(86_400);

        vm.expectEmit(true, false, false, false);
        emit TradingFeesUpdated(Id.wrap(1), 0, 0);
        manager.updateFees();

        manager.proposeFees(50_000_000, 50_000_000);
        skip(86_400);

        vm.expectEmit(true, false, false, false);
        emit TradingFeesUpdated(Id.wrap(2), 0, 0);
        manager.updateFees();

        vm.stopPrank();
    }
}
