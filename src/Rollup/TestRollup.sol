// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2023 TXA PTE. LTD.
pragma solidity 0.8.19;

import "./Rollup.sol";

contract TestRollup is Rollup {
    constructor(address _participatingInterface, address _manager) Rollup(_participatingInterface, _manager) {}

    function testPrepareSettlement(
        bytes32 _stateRoot,
        StateUpdateLibrary.SignedStateUpdate calldata _signedUpdate
    ) external returns (StateUpdateLibrary.Settlement memory) {
        return prepareSettlement(_stateRoot, _signedUpdate);
    }
}
