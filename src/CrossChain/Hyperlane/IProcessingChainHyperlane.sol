// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2023 TXA PTE. LTD.
pragma solidity 0.8.19;

import "../../Portal/IPortal.sol";

interface IProcessingChainHyperlane {
    function sendObligations(
        uint256 _chainId,
        IPortal.Obligation[] calldata _obligations,
        address _refundAddress
    )
        external
        payable;

    function sendDepositRejections(
        uint256 _chainId,
        bytes32[] calldata _depositHashes,
        address _refunedAddress
    )
        external
        payable;
}
