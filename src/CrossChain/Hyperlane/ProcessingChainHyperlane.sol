// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2023 TXA PTE. LTD.
pragma solidity 0.8.19;

import "../CrossChainFunctions.sol";
import "./IProcessingChainHyperlane.sol";
import "../LayerZero/IProcessingChainLz.sol";
import "../../Manager/ProcessingChain/IProcessingChainManager.sol";
import "../../Manager/AssetChain/IAssetChainManager.sol";
import "@Hyperlane/client/MailboxClient.sol";
import "@Hyperlane/libs/TypeCasts.sol";

/**
 * Deploys on the processing chain and handles sending/receiving messages using LayerZero
 */
contract ProcessingChainHyperlane is MailboxClient, IProcessingChainHyperlane, IProcessingChainLz, CrossChainFunctions {
    IProcessingChainManager public manager;
    IAssetChainManager public assetManager;

    // Maps EVM chain ID to LayerZero chain ID
    mapping(uint256 => uint32) chainIds;
    mapping(uint256 => bytes32) portals;

    constructor(
        address _hyperlaneMailbox,
        address _owner,
        address _manager,
        address _assetManager
    )
        MailboxClient(_hyperlaneMailbox)
    {
        _transferOwnership(_owner);
        manager = IProcessingChainManager(_manager);
        assetManager = IAssetChainManager(_assetManager);
    }

    function setChainIds(uint256[] calldata evmChainId, uint32[] calldata hyperlaneChainId) external onlyOwner {
        if (evmChainId.length != hyperlaneChainId.length) revert("Lengths of chain ID arrays don't match");
        for (uint256 i = 0; i < evmChainId.length; i++) {
            chainIds[evmChainId[i]] = hyperlaneChainId[i];
        }
    }

    function setReceiverAddresses(uint32[] calldata hyperlaneChainId, address[] calldata receiverAddress) external onlyOwner {
      if (hyperlaneChainId.length != receiverAddress.length) revert("Lengths of arrays don't match");
      for (uint256 i = 0; i < hyperlaneChainId.length; i++) {
        portals[hyperlaneChainId[i]] = TypeCasts.addressToBytes32(receiverAddress[i]);
      }
    }

    // Send obligations to Portal via AssetChainHyperlane on specified chain
    // Used after processing settlements, trading fees, and staking rewards
    function sendObligations(
        uint256 _chainId,
        IPortal.Obligation[] calldata _obligations,
        address _refundAddress
    )
        external
        payable
    {
        if (!(msg.sender == manager.rollup() || msg.sender == manager.staking() || msg.sender == address(this))) {
            revert("Sender must be Rollup or Staking contract");
        }
        // If processing settlement for same chain as processing chain, call Portal directly
        // We expect this contract to be authorized in AssetChainManager
        if (_chainId == block.chainid) {
            IPortal(assetManager.portal()).writeObligations(_obligations);
            if (msg.value > 0) {
                (bool success,) = _refundAddress.call{ value: msg.value }("");
                if (!success) revert("failed to refund");
            }
            return;
        }
        uint32 hyperlaneChainId = chainIds[_chainId];
        bytes32 portal = portals[_chainId];
        if (hyperlaneChainId == 0) revert("HyperlaneChainId not set");
        if (portal == bytes32(0)) revert("Portal not set");
        bytes memory payload = abi.encode(CrossChainMessage(WRITE_OBLIGATIONS, abi.encode(_obligations)));
        uint256 cost = _quoteDispatch(hyperlaneChainId, portal, payload);
        uint256 refund = msg.value - cost;
        payable(tx.origin).transfer(refund);
        _dispatch(hyperlaneChainId, portal, cost, payload);
    }

    // For compatibility with the contracts which rely on the IProcessingChainLz
    function sendObligations(
        uint256 _chainId,
        IPortal.Obligation[] calldata _obligations,
        bytes calldata _ignored,
        address _refundAddress
    )
        external
        payable
    {
        this.sendObligations{value: msg.value}(_chainId, _obligations, _refundAddress);
    }

    function sendDepositRejections(
        uint256 _chainId,
        bytes32[] calldata _depositHashes,
        address _refundAddress
    )
        external
        payable
    {
        if (!(msg.sender == manager.rollup() || msg.sender == address(this))) revert("Sender must be Rollup");
        // If rejecting deposits for same chain as processing chain, call Portal directly
        // We expect this contract to be authorized in AssetChainManager
        if (_chainId == block.chainid) {
            IPortal(assetManager.portal()).rejectDeposits(_depositHashes);
            if (msg.value > 0) {
                (bool success,) = _refundAddress.call{ value: msg.value }("");
                if (!success) revert("failed to refund");
            }
            return;
        }

        // TODO: Here we would check whether the message must be processed via
        // Hyperlane or LayerZero, and dispatch the corresponding msg
        uint32 hyperlaneChainId = chainIds[_chainId];
        bytes32 portal = portals[_chainId];
        if (hyperlaneChainId == 0) revert("HyperlaneChainId not set");
        if (portal == bytes32(0)) revert("Portal not set");
        bytes memory payload = abi.encode(CrossChainMessage(REJECT_DEPOSITS, abi.encode(_depositHashes)));
        uint256 cost = _quoteDispatch(hyperlaneChainId, portal, payload);
        uint256 refund = msg.value - cost;
        payable(tx.origin).transfer(refund);
        _dispatch(hyperlaneChainId, portal, cost, payload);
    }

    // For compatibility with the contracts which rely on the IProcessingChainLz
    function sendDepositRejections(
        uint256 _chainId,
        bytes32[] calldata _depositHashes,
        bytes calldata _ignored,
        address _refundAddress
    )
        external
        payable
    {
        this.sendDepositRejections(_chainId, _depositHashes, _refundAddress);
    }
}
