// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2023 TXA PTE. LTD.
pragma solidity 0.8.19;

import "../CrossChainFunctions.sol";
import "../../Portal/IPortal.sol";
import "../../Manager/AssetChain/IAssetChainManager.sol";
import "@Hyperlane/interfaces/IMessageRecipient.sol";
import "@Hyperlane/client/MailboxClient.sol";

/**
 * Deploys on the asset chain and handles sending/receiving messages using Hyperlane
 */
contract AssetChainHyperlane is MailboxClient, IMessageRecipient, CrossChainFunctions {
    address public manager;
    uint32 public immutable processingChainId;

    constructor(address _admin, address _manager, address _hyperlaneMailbox, uint32 _processingChainId) MailboxClient(_hyperlaneMailbox) {
        manager = _manager;
        _transferOwnership(_admin);
        processingChainId = _processingChainId;
    }

    function handle(
      uint32 _origin,
      bytes32 _sender,
      bytes calldata _data
    ) external payable virtual override onlyMailbox {
      if (_origin != processingChainId) revert();
      CrossChainMessage memory message = abi.decode(_data, (CrossChainMessage));
      if (message.instruction == WRITE_OBLIGATIONS) {
        IPortal.Obligation[] memory obligations = abi.decode(message.payload, (IPortal.Obligation[]));
        IPortal(IAssetChainManager(manager).portal()).writeObligations(obligations);
      } else if (message.instruction == REJECT_DEPOSITS) {
        bytes32[] memory depositHashes = abi.decode(message.payload, (bytes32[]));
        IPortal(IAssetChainManager(manager).portal()).rejectDeposits(depositHashes);
      }
    }
}
